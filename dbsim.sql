-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2017 at 03:05 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsim`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses`
--

CREATE TABLE `akses` (
  `id_akses` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_unit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses`
--

INSERT INTO `akses` (`id_akses`, `id_user`, `id_unit`) VALUES
(1, 4, 25),
(2, 2, 23),
(3, 5, NULL),
(4, 6, 23);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `namajabatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `namajabatan`) VALUES
(31, 'Ketua Unit'),
(32, 'Staff'),
(33, 'Pelaksana');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `namakriteria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `namakriteria`) VALUES
(1, 'Tanggung Jawab'),
(2, 'Komunikasi'),
(3, 'Disiplin'),
(4, 'Perilaku'),
(5, 'Kehadiran'),
(6, 'Loyalitas'),
(7, 'Pendidikan');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `id_mutasi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_unit` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi`
--

INSERT INTO `mutasi` (`id_mutasi`, `tanggal`, `id_unit`, `id_jabatan`) VALUES
(1, '2017-07-25', 27, 33);

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_detail`
--

CREATE TABLE `mutasi_detail` (
  `id_md` int(11) NOT NULL,
  `id_mutasi` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi_detail`
--

INSERT INTO `mutasi_detail` (`id_md`, `id_mutasi`, `id_pegawai`) VALUES
(1, 1, 22),
(2, 1, 29),
(3, 1, 24),
(4, 1, 25),
(5, 1, 23);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nip` int(7) NOT NULL,
  `namapegawai` varchar(30) NOT NULL,
  `tempatlahir` varchar(25) NOT NULL,
  `tanggallahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jeniskelamin` enum('Pria','Wanita') NOT NULL,
  `goldarah` enum('A','B','AB','O') NOT NULL,
  `pendidikan` int(11) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nip`, `namapegawai`, `tempatlahir`, `tanggallahir`, `alamat`, `jeniskelamin`, `goldarah`, `pendidikan`, `jabatan`, `unit`, `id_user`) VALUES
(22, 1006142, 'Eri Handayani', 'CIREBON', '1984-12-14', 'KEBON PRING NO. 61 RT/RW. 01/05 PEKALIPAN KEC. PEKALIPAN', 'Wanita', 'O', 4, 31, 25, NULL),
(23, 1109261, 'Syamsuri', 'Cirebon', '1987-07-07', 'BLOK CILUWUNG RT. 03/RW. 05 DESA KEDUNGBUNDER KEC. GEMPOL', 'Pria', 'B', 8, 32, 25, NULL),
(24, 1007011, 'Sigit Herlambang', 'Cirebon', '1986-03-16', 'JL. ADIPURA LESTARI 2 NO.18 BCA CIREBON RT/RW 02/08', 'Pria', 'O', 4, 31, 23, NULL),
(25, 1006146, 'Farikhin', 'Cirebon', '1986-09-16', 'BLOK PASEK RT/RW. 38/09 PALIMANAN BARAT KEC. PALIMANAN', 'Pria', 'B', 8, 33, 23, NULL),
(29, 1006521, 'Rifqi', 'BEKASI', '1999-04-14', 'KAYURINGIN', 'Pria', 'AB', 4, 31, 25, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `namapendidikan` varchar(15) NOT NULL,
  `tingkatan` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `namapendidikan`, `tingkatan`) VALUES
(3, 'S1 H', 3),
(4, 'S1', 3),
(5, 'D3 H', 2),
(6, 'D3', 2),
(7, 'SMA H / SMK H', 1),
(8, 'SMA/SMK', 1),
(9, 'IKOPIN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jabatan`
--

CREATE TABLE `pendidikan_jabatan` (
  `id_pj` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_pendidikan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jabatan`
--

INSERT INTO `pendidikan_jabatan` (`id_pj`, `id_jabatan`, `id_pendidikan`) VALUES
(1, 31, 3),
(2, 31, 4),
(3, 32, 5),
(4, 32, 6),
(5, 33, 7),
(6, 33, 8);

-- --------------------------------------------------------

--
-- Table structure for table `penilaiankriteria`
--

CREATE TABLE `penilaiankriteria` (
  `id_pk` int(11) NOT NULL,
  `id_kriteria_1` int(11) NOT NULL,
  `id_kriteria_2` int(11) NOT NULL,
  `nilai` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaiankriteria`
--

INSERT INTO `penilaiankriteria` (`id_pk`, `id_kriteria_1`, `id_kriteria_2`, `nilai`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 3),
(3, 1, 3, 3),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 7, 1),
(8, 2, 1, 0.33),
(9, 2, 2, 1),
(10, 2, 3, 5),
(11, 2, 4, 1),
(12, 2, 5, 1),
(13, 2, 6, 1),
(14, 2, 7, 1),
(15, 3, 1, 0.33),
(16, 3, 2, 0.2),
(17, 3, 3, 1),
(18, 3, 4, 1),
(19, 3, 5, 1),
(20, 3, 6, 1),
(21, 3, 7, 1),
(22, 4, 1, 1),
(23, 4, 2, 1),
(24, 4, 3, 1),
(25, 4, 4, 1),
(26, 4, 5, 3),
(27, 4, 6, 1),
(28, 4, 7, 1),
(29, 5, 1, 1),
(30, 5, 2, 1),
(31, 5, 3, 1),
(32, 5, 4, 0.33),
(33, 5, 5, 1),
(34, 5, 6, 1),
(35, 5, 7, 1),
(36, 6, 1, 1),
(37, 6, 2, 1),
(38, 6, 3, 1),
(39, 6, 4, 1),
(40, 6, 5, 1),
(41, 6, 6, 1),
(42, 6, 7, 7),
(43, 7, 1, 1),
(44, 7, 2, 1),
(45, 7, 3, 1),
(46, 7, 4, 1),
(47, 7, 5, 1),
(48, 7, 6, 0.14),
(49, 7, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penilaianpegawai`
--

CREATE TABLE `penilaianpegawai` (
  `id_pg` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaianpegawai`
--

INSERT INTO `penilaianpegawai` (`id_pg`, `id_pegawai`, `tahun`) VALUES
(1, 22, 2017),
(2, 23, 2017),
(3, 29, 2017);

-- --------------------------------------------------------

--
-- Table structure for table `prioritas_kriteria`
--

CREATE TABLE `prioritas_kriteria` (
  `id_prioritas` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `nilai` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `prioritas_kriteria`
--

INSERT INTO `prioritas_kriteria` (`id_prioritas`, `id_kriteria`, `nilai`) VALUES
(1, 1, '0.18'),
(2, 2, '0.15'),
(3, 3, '0.10'),
(4, 4, '0.16'),
(5, 5, '0.11'),
(6, 6, '0.19'),
(7, 7, '0.11');

-- --------------------------------------------------------

--
-- Table structure for table `promosi`
--

CREATE TABLE `promosi` (
  `id_promosi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_unit` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promosi_detail`
--

CREATE TABLE `promosi_detail` (
  `id_pd` int(11) NOT NULL,
  `id_promosi` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `p_pegawai_detail`
--

CREATE TABLE `p_pegawai_detail` (
  `id_pg_detail` int(11) NOT NULL,
  `id_pg` int(11) NOT NULL,
  `id_prioritas` int(11) NOT NULL,
  `nilai` int(3) UNSIGNED NOT NULL,
  `subtotal_nilai` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_pegawai_detail`
--

INSERT INTO `p_pegawai_detail` (`id_pg_detail`, `id_pg`, `id_prioritas`, `nilai`, `subtotal_nilai`) VALUES
(1, 1, 1, 60, '10.80'),
(2, 1, 2, 70, '10.50'),
(3, 1, 3, 66, '6.60'),
(4, 1, 4, 78, '12.48'),
(5, 1, 5, 69, '7.59'),
(6, 1, 6, 66, '12.54'),
(7, 1, 7, 76, '8.36'),
(8, 2, 1, 78, '14.04'),
(9, 2, 2, 66, '9.90'),
(10, 2, 3, 86, '8.60'),
(11, 2, 4, 54, '8.64'),
(12, 2, 5, 65, '7.15'),
(13, 2, 6, 77, '14.63'),
(14, 2, 7, 78, '8.58'),
(15, 3, 1, 90, '16.20'),
(16, 3, 2, 89, '13.35'),
(17, 3, 3, 88, '8.80'),
(18, 3, 4, 76, '12.16'),
(19, 3, 5, 54, '5.94'),
(20, 3, 6, 66, '12.54'),
(21, 3, 7, 90, '9.90');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id_unit` int(11) NOT NULL,
  `namaunit` varchar(30) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `is_khusus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id_unit`, `namaunit`, `id_jabatan`, `is_khusus`) VALUES
(21, 'TB', NULL, 0),
(22, 'Banyu Panas', NULL, 0),
(23, 'Toko', NULL, 0),
(24, 'Jasa Civil', NULL, 0),
(25, 'Dagang Umum', NULL, 0),
(26, 'Administrasi dan Accounting', NULL, 1),
(27, 'Ketenagakerjaan', NULL, 0),
(28, 'Simpan Pinjam', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('KETUAKOPERASI','KETUAUNIT','KETENAGAKERJAAN') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `status`) VALUES
(2, 'Feisal', 'feisal', '99b014851d8604fceb3a614c95092a55', 'KETENAGAKERJAAN'),
(4, 'Akbar', 'akbar', '4f033a0a2bf2fe0b68800a3079545cd1', 'KETUAUNIT'),
(5, 'Fauziah', 'fauziah', 'a8e404443f4edea9337a04fa3bf192b1', 'KETUAKOPERASI'),
(6, 'Tangguh', 'tangguh', '1541f132320c79deec969b312c5ab5f4', 'KETUAUNIT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id_akses`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_unit` (`id_unit`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`id_mutasi`),
  ADD KEY `id_unit` (`id_unit`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `mutasi_detail`
--
ALTER TABLE `mutasi_detail`
  ADD PRIMARY KEY (`id_md`),
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_mutasi` (`id_mutasi`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_pendidikan` (`pendidikan`),
  ADD KEY `unit` (`unit`),
  ADD KEY `jabatan` (`jabatan`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pendidikan_jabatan`
--
ALTER TABLE `pendidikan_jabatan`
  ADD PRIMARY KEY (`id_pj`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_pendidikan` (`id_pendidikan`);

--
-- Indexes for table `penilaiankriteria`
--
ALTER TABLE `penilaiankriteria`
  ADD PRIMARY KEY (`id_pk`),
  ADD KEY `id_kriteria_1` (`id_kriteria_1`),
  ADD KEY `id_kriteria_2` (`id_kriteria_2`);

--
-- Indexes for table `penilaianpegawai`
--
ALTER TABLE `penilaianpegawai`
  ADD PRIMARY KEY (`id_pg`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `prioritas_kriteria`
--
ALTER TABLE `prioritas_kriteria`
  ADD PRIMARY KEY (`id_prioritas`),
  ADD KEY `id_kriteria` (`id_kriteria`);

--
-- Indexes for table `promosi`
--
ALTER TABLE `promosi`
  ADD PRIMARY KEY (`id_promosi`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_unit` (`id_unit`);

--
-- Indexes for table `promosi_detail`
--
ALTER TABLE `promosi_detail`
  ADD PRIMARY KEY (`id_pd`),
  ADD KEY `id_promosi` (`id_promosi`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `p_pegawai_detail`
--
ALTER TABLE `p_pegawai_detail`
  ADD PRIMARY KEY (`id_pg_detail`),
  ADD KEY `id_eigen` (`id_prioritas`),
  ADD KEY `id_pg` (`id_pg`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id_unit`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id_akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mutasi`
--
ALTER TABLE `mutasi`
  MODIFY `id_mutasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mutasi_detail`
--
ALTER TABLE `mutasi_detail`
  MODIFY `id_md` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pendidikan_jabatan`
--
ALTER TABLE `pendidikan_jabatan`
  MODIFY `id_pj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `penilaiankriteria`
--
ALTER TABLE `penilaiankriteria`
  MODIFY `id_pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `penilaianpegawai`
--
ALTER TABLE `penilaianpegawai`
  MODIFY `id_pg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prioritas_kriteria`
--
ALTER TABLE `prioritas_kriteria`
  MODIFY `id_prioritas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `promosi`
--
ALTER TABLE `promosi`
  MODIFY `id_promosi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `promosi_detail`
--
ALTER TABLE `promosi_detail`
  MODIFY `id_pd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `p_pegawai_detail`
--
ALTER TABLE `p_pegawai_detail`
  MODIFY `id_pg_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id_unit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_2` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`) ON UPDATE CASCADE,
  ADD CONSTRAINT `akses_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD CONSTRAINT `mutasi_ibfk_1` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mutasi_ibfk_2` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mutasi_detail`
--
ALTER TABLE `mutasi_detail`
  ADD CONSTRAINT `mutasi_detail_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mutasi_detail_ibfk_2` FOREIGN KEY (`id_mutasi`) REFERENCES `mutasi` (`id_mutasi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `pegawai_ibfk_5` FOREIGN KEY (`pendidikan`) REFERENCES `pendidikan` (`id_pendidikan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pegawai_ibfk_6` FOREIGN KEY (`unit`) REFERENCES `unit` (`id_unit`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pegawai_ibfk_7` FOREIGN KEY (`jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON UPDATE CASCADE;

--
-- Constraints for table `pendidikan_jabatan`
--
ALTER TABLE `pendidikan_jabatan`
  ADD CONSTRAINT `pendidikan_jabatan_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendidikan_jabatan_ibfk_2` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan` (`id_pendidikan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penilaiankriteria`
--
ALTER TABLE `penilaiankriteria`
  ADD CONSTRAINT `penilaiankriteria_ibfk_1` FOREIGN KEY (`id_kriteria_1`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penilaiankriteria_ibfk_2` FOREIGN KEY (`id_kriteria_2`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penilaianpegawai`
--
ALTER TABLE `penilaianpegawai`
  ADD CONSTRAINT `penilaianpegawai_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`);

--
-- Constraints for table `prioritas_kriteria`
--
ALTER TABLE `prioritas_kriteria`
  ADD CONSTRAINT `prioritas_kriteria_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `promosi`
--
ALTER TABLE `promosi`
  ADD CONSTRAINT `promosi_ibfk_1` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `promosi_ibfk_2` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `promosi_detail`
--
ALTER TABLE `promosi_detail`
  ADD CONSTRAINT `promosi_detail_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `promosi_detail_ibfk_3` FOREIGN KEY (`id_promosi`) REFERENCES `promosi` (`id_promosi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_pegawai_detail`
--
ALTER TABLE `p_pegawai_detail`
  ADD CONSTRAINT `p_pegawai_detail_ibfk_1` FOREIGN KEY (`id_pg`) REFERENCES `penilaianpegawai` (`id_pg`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unit`
--
ALTER TABLE `unit`
  ADD CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
