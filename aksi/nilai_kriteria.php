<?php
    //include_once("conf.php");

    function save_nilai_kriteria($conn, $kriteria) 
    {
        mysqli_query($conn, "truncate table penilaiankriteria"); /* kosongkan tabel nilai_kriteria */

        // Looping seluruh kriteria
        for($i=0;$i<count($kriteria);$i++){
            for($ii=0;$ii<count($kriteria);$ii++){
                if($i < $ii){
                    mysqli_query($conn, "insert into penilaiankriteria(id_kriteria_1,id_kriteria_2,nilai) values('".$kriteria[$i][0]."','".$kriteria[$ii][0]."','".$_POST['nilai_'.$kriteria[$i][0].'_'.$kriteria[$ii][0]]."')");
                }
            }
        }
        $success='Nilai perbandingan kriteria berhasil disimpan.';
    }
    
    function update_nilai_kriteria($conn)
    {
        $kriteria1 = $_POST["kriteria1"];
        $kriteria2 = $_POST["kriteria2"];
        $perbandingan = $_POST["perbandingan"];
        $nilaibalik = round(1 / floatval($perbandingan), 2);
        $result1 = mysqli_query($conn, "UPDATE penilaiankriteria SET nilai = '". $perbandingan."' WHERE id_kriteria_1 = '". $kriteria1 ."' AND id_kriteria_2 = '". $kriteria2 ."'");
        $result2 = mysqli_query($conn, "UPDATE penilaiankriteria SET nilai = '". $nilaibalik."' WHERE id_kriteria_1 = '". $kriteria2 ."' AND id_kriteria_2 = '". $kriteria1 ."'");

        return ($result1 && $result2);
    }
?>