<?php

/*
	Fungsi untuk mendapatkan matrik dari kriteria
	param : $id_kriteria => array yang isinya id_kriteria. Contoh:
				$id_kriteria[0] = "02" -> Kerajinan
				$id_kriteria[0] = "03" -> Kedisiplinan
*/
function ahp_get_matrik_kriteria($conn, $id_kriteria){

	//Looping seluruh Kriteria
	for($i=0;$i<count($id_kriteria);$i++){
		for($ii=0;$ii<count($id_kriteria);$ii++){
			//Check kalo sama kan nilainya satu
			if($i==$ii){
				$matrik[$i][$ii]=1;
			}else{
				// Check 0 < 1 dst..
				if($i < $ii){
					$q=mysqli_query($conn, "select nilai from penilaiankriteria where id_kriteria_1='".$id_kriteria[$i]."' and id_kriteria_2='".$id_kriteria[$ii]."'");
					if(mysqli_num_rows($q)>0){
						$h=mysqli_fetch_array($q);
						$nilai=$h['nilai'];

						//Hitung nilai. Misal kriteria a 2x dari b. maka a = 2, b = 1/2 (0,5000 => dibulatkan 4 dibelakang koma)
						$matrik[$i][$ii]=$nilai;
						$matrik[$ii][$i]=round((1/$nilai),4);
					}else{
						//Set nilai jadi sama dengan kalo gk ada di db
						$matrik[$i][$ii]=1;
						$matrik[$ii][$i]=1;
					}
				}
			}
		}
	}
	return $matrik;
}

/*
	Fungsi untuk menjumlahkan setiap kolom dari matrix
	Tahap Kedua dari teori AHP
	Param : $matrik => Matrik yang dibutuhkan untuk dijumlahkan kolom2 nya
*/
function ahp_get_jumlah_kolom($matrik){
	for($i=0;$i<count($matrik);$i++){
		$jumlah_kolom[$i] = 0;
		for($ii=0;$ii<count($matrik);$ii++){
			$jumlah_kolom[$i] = $jumlah_kolom[$i] + $matrik[$ii][$i];
		}
	}
	return $jumlah_kolom;
}

/*
	Fungsi untuk menjumlahkan setiap baris dari matrix dan menormalisasikannya
	Tahap Ketiga dari teori AHP
	Param : $matrik => Matrik yang dibutuhkan untuk dijumlahkan kolom2 nya
			$jumlah_kolom => Jumlah dari setiap kolom matrik yang bersangkutan
*/
function ahp_get_normalisasi($matrik, $jumlah_kolom){
	for($i=0;$i<count($matrik);$i++){
		for($ii=0;$ii<count($matrik);$ii++){
			$matrik_normalisasi[$i][$ii] = round( $matrik[$i][$ii] / $jumlah_kolom[$ii] , 4 );
		}
	}
	return $matrik_normalisasi;
}

/*
	Fungsi untuk menjumlahkan setiap kolom dari matrix
	Tahap Teakhir dari teori AHP
	Param : $matrik_normalisasi => Matrik hasil normalisasi yang dibutuhkan untuk diambil eigennya
*/
function ahp_get_eigen($matrik_normalisasi){
	for($i=0;$i<count($matrik_normalisasi);$i++){
		$eigen[$i] = 0;
		for($ii=0;$ii<count($matrik_normalisasi);$ii++){
			$eigen[$i] = $eigen[$i] + $matrik_normalisasi[$i][$ii];
		}
		$eigen[$i] = round( $eigen[$i] / count($matrik_normalisasi) , 4 );
	}
	return $eigen;
}

function ahp_uji_konsistensi($matrik, $eigen){
	for($i=0;$i<count($matrik);$i++){
		$nilai=0;
		for($ii=0;$ii<count($matrik);$ii++){
			$nilai = $nilai + ($matrik[$i][$ii] * $eigen[$ii]);
		}
		$matrik_eigen[$i] = $nilai;
	}
	$nilai=0;
	for($i=0;$i<count($matrik);$i++){
		$nilai = $nilai + ($matrik_eigen[$i] / $eigen[$i]);
	}
	$t = $nilai / count($matrik);
	$ci = ($t - count($matrik)) / (count($matrik)-1);

	//Ini berdasarkan tabel yang ada di teori AHP (Fixed)
	$ri=array(0,0,0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 1.51, 1.48, 1.56, 1.57, 1.59);
	$cr = $ci / $ri[count($matrik)];

	//Check konsistensi
	if($cr <= 0.1){
		return true;
	}else{
		return false;
	}
}
?>