-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2017 at 12:35 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsim`
--

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `namajabatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `namajabatan`) VALUES
(31, 'Ketua Unit'),
(32, 'Staff'),
(33, 'Pelaksana');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `namakriteria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `namakriteria`) VALUES
(1, 'Tanggung Jawab'),
(2, 'Disiplin'),
(3, 'Komunikasi'),
(4, 'Kehadiran'),
(5, 'Loyalitas'),
(6, 'Perilaku'),
(7, 'Pendidikan');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `id_mutasi` int(11) NOT NULL,
  `namapegawai` varchar(25) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_pg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nip` int(7) NOT NULL,
  `namapegawai` varchar(30) NOT NULL,
  `tempatlahir` varchar(25) NOT NULL,
  `tanggallahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jeniskelamin` enum('Pria','Wanita') NOT NULL,
  `goldarah` enum('A','B','AB','O') NOT NULL,
  `pendidikan` enum('S1','D3','IKOPIN','SMA','STM','SLTA') NOT NULL,
  `jabatan` enum('Ketua Unit','Staff','Pelaksana') NOT NULL,
  `unit` enum('TOKO','TB','DAGANG UMUM','JASA CIVIL','BANYU PANAS','SIMPAN PINJAM','KETENAGAKERJAAN') NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_unit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nip`, `namapegawai`, `tempatlahir`, `tanggallahir`, `alamat`, `jeniskelamin`, `goldarah`, `pendidikan`, `jabatan`, `unit`, `id_jabatan`, `id_user`, `id_unit`) VALUES
(21, 1006141, 'Agung Ismaya', 'Cirebon', '1984-09-24', 'DATA KARYAWAN KOPKAR MANUNGGAL PERKASA 2017\r\nNO	NIK	NAMA	TEMPAT LAHIR	TANGGAL LAHIR	L/P	ALAMAT\r\n1	10', 'Pria', 'O', 'S1', 'Staff', 'DAGANG UMUM', NULL, NULL, NULL),
(22, 1006142, 'Eri Handayani', 'CIREBON', '1984-12-14', 'KEBON PRING NO. 61 RT/RW. 01/05 PEKALIPAN KEC. PEKALIPAN', 'Wanita', 'O', 'S1', 'Staff', 'DAGANG UMUM', NULL, NULL, NULL),
(23, 1109261, 'Syamsuri', 'Cirebon', '1987-07-07', 'BLOK CILUWUNG RT. 03/RW. 05 DESA KEDUNGBUNDER KEC. GEMPOL', 'Pria', 'B', 'STM', 'Pelaksana', 'DAGANG UMUM', NULL, NULL, NULL),
(24, 1007011, 'Sigit Herlambang', 'Cirebon', '1986-03-16', 'JL. ADIPURA LESTARI 2 NO.18 BCA CIREBON RT/RW 02/08', 'Pria', 'O', 'S1', 'Staff', 'KETENAGAKERJAAN', NULL, NULL, NULL),
(25, 1006146, 'Farikhin', 'Cirebon', '1986-09-16', 'BLOK PASEK RT/RW. 38/09 PALIMANAN BARAT KEC. PALIMANAN', 'Pria', 'B', 'SMA', 'Pelaksana', 'KETENAGAKERJAAN', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `namapendidikan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `namapendidikan`) VALUES
(3, 'S1 H'),
(4, 'S1'),
(5, 'D3 H'),
(6, 'D3'),
(7, 'SMA H / SMK H'),
(8, 'SMA/SMK'),
(9, 'IKOPIN');

-- --------------------------------------------------------

--
-- Table structure for table `penilaiankriteria`
--

CREATE TABLE `penilaiankriteria` (
  `id_pk` int(11) NOT NULL,
  `id_kriteria_1` int(11) NOT NULL,
  `id_kriteria_2` int(11) NOT NULL,
  `nilai` float NOT NULL,
  `id_kriteria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penilaianpegawai`
--

CREATE TABLE `penilaianpegawai` (
  `id_pg` int(11) NOT NULL,
  `namapegawai` varchar(30) NOT NULL,
  `namakriteria` varchar(25) NOT NULL,
  `nilai` int(10) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_pk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promosi`
--

CREATE TABLE `promosi` (
  `id_promosi` int(11) NOT NULL,
  `namapegawai` varchar(30) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_pg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id_unit` int(11) NOT NULL,
  `namaunit` varchar(30) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id_unit`, `namaunit`, `id_jabatan`) VALUES
(21, 'TB', NULL),
(22, 'Banyu Panas', NULL),
(23, 'Toko', NULL),
(24, 'Jasa Civil', NULL),
(25, 'Dagang Umum', NULL),
(26, 'Administrasi dan Accounting', NULL),
(27, 'Ketenagakerjaan', NULL),
(28, 'Simpan Pinjam', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('KETUAKOPERASI','KETUAUNIT','KETENAGAKERJAAN') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `status`) VALUES
(2, 'Feisal', 'feisal', '99b014851d8604fceb3a614c95092a55', 'KETENAGAKERJAAN'),
(3, 'Fauzi', 'fauzi', 'e10adc3949ba59abbe56e057f20f883e', 'KETUAKOPERASI'),
(4, 'Akbar', 'akbar', 'e10adc3949ba59abbe56e057f20f883e', 'KETUAUNIT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_pg` (`id_pg`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_unit` (`id_unit`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `penilaiankriteria`
--
ALTER TABLE `penilaiankriteria`
  ADD PRIMARY KEY (`id_pk`),
  ADD KEY `id_kriteria` (`id_kriteria`);

--
-- Indexes for table `penilaianpegawai`
--
ALTER TABLE `penilaianpegawai`
  ADD PRIMARY KEY (`id_pg`),
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_pk` (`id_pk`);

--
-- Indexes for table `promosi`
--
ALTER TABLE `promosi`
  ADD KEY `promosi_ibfk_1` (`id_pg`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id_unit`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id_unit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD CONSTRAINT `mutasi_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`),
  ADD CONSTRAINT `mutasi_ibfk_2` FOREIGN KEY (`id_pg`) REFERENCES `penilaianpegawai` (`id_pg`);

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_ibfk_1` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`),
  ADD CONSTRAINT `pegawai_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `pegawai_ibfk_4` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`);

--
-- Constraints for table `penilaiankriteria`
--
ALTER TABLE `penilaiankriteria`
  ADD CONSTRAINT `penilaiankriteria_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id_kriteria`);

--
-- Constraints for table `penilaianpegawai`
--
ALTER TABLE `penilaianpegawai`
  ADD CONSTRAINT `penilaianpegawai_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`),
  ADD CONSTRAINT `penilaianpegawai_ibfk_2` FOREIGN KEY (`id_pk`) REFERENCES `penilaiankriteria` (`id_pk`);

--
-- Constraints for table `promosi`
--
ALTER TABLE `promosi`
  ADD CONSTRAINT `promosi_ibfk_1` FOREIGN KEY (`id_pg`) REFERENCES `penilaianpegawai` (`id_pg`),
  ADD CONSTRAINT `promosi_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`);

--
-- Constraints for table `unit`
--
ALTER TABLE `unit`
  ADD CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
