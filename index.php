<?php
  session_start();
  if(!isset($_SESSION["username"])){ header('Location: login.php'); }
  include_once("conf.php");
  include_once("confjs.php");
  include_once("lib.php");
  include_once("aksi/nilai_kriteria.php");
  include_once("aksi/ahp.php");
  include_once("aksi/penilaian_pegawai.php");
  $page = isset($_GET['page']) ?$_GET['page']:"konten";
  $page_ ="modul/".$page.".php";

?>

<!DOCTYPE html>
<html>
 <head>
     <meta charset="UTF-8">
     <title>Koperasi Karyawan Manunggal Perkasa</title>
     <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
     <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/morris/morris.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
     <link href="assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

     <!-- script -->
     <script src="assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
     <script src="assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
     <script src="assets/js/plugins/morris/morris.min.js" type="text/javascript"></script>
     <script src="assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
     <script src="assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
     <script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
     <script src="assets/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
     <script src="assets/js/AdminLTE/app.js" type="text/javascript"></script>
     <script src="assets/js/AdminLTE/dashboard.js" type="text/javascript"></script>
     <script src="assets/js/AdminLTE/demo.js" type="text/javascript"></script>
     <script src="assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
     <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
     <!-- selesai script -->
 </head>

    <body class="pace-done skin-blue fixed">
        <!-- header -->
        <header class="header">
            <a href="#" class="logo" >KOPKAR<span style="color:transparent;">.....</span></a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- navigasi menu-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigations</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <!-- selesai navigasi menu -->

                <!-- notifikasi kanan -->
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span> <?php echo $_SESSION['nama']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                               <!-- Menu Footer-->
                                <li class="user-footer ">
                                    <div class="text-center">
                                        <a href="login.php?log=logout" class="btn btn-default btn-flat" onclick="remove()">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- selesai notifikasi kanan -->

            </nav>
            <!-- selesai header navbar -->

        </header>
        <!-- selesai header -->

        <!-- body -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
          <?php
            include_once("sidebar_kiri.php");//menu dan header
           ?>

           <!-- sidebar kanan konten -->
           <aside class="right-side">

               <!-- konten -->
               <section class="content">
                   <?php
                     include_once($page_);
                    ?>
               </section>
               <!-- selesai konten -->
           </aside>
           <!-- selesai sidebar kanan konten -->

        </div>
        <!-- selesai body -->

    </body>
</html>

<script type="text/javascript">
function remove(){
  sessionStorage.removeItem("unit");
  sessionStorage.removeItem("cariunit");
}