
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php
  $kriteria=array();
  $q="select * from kriteria order by id_kriteria";
  $q=mysqli_query($conn, $q);
  while($h=mysqli_fetch_array($q)){

    // Push ke array kriteria
    // Format Array = [id_kriteria, id_kriteria, nama kriteria]
    // $kriteria[0][2] => muncul namanya
    $kriteria[]=array($h['id_kriteria'],$h['id_kriteria'],$h['namakriteria']);
  }
?>

<!-- Modal -->
<div class="modal fade " role="dialog" id="myModal">
  <div class=" modal-dialog">
    <div class="box box-solid box-primary modal-content">

      <div class="box-header">
        <h3 class="box-title"><i class="ion-person-add"></i> &nbsp; kriteria</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
        </div>
      </div>

      <div class="box-body">
        <form action="" method="post" id="formiki">
          <input type="hidden" id="id_pg" name="id_pg" value="">
            <?php
              $query = "SELECT * FROM kriteria WHERE NOT namakriteria = 'Pendidikan'";
              $data = mysqli_query($conn, $query);
              while ($row = mysqli_fetch_assoc($data)) {
            ?>
          
            <div class="form-group">
              <label ><?= $row["namakriteria"] ?></label>
              <input disabled type="number" min="0" class="form-control" placeholder='<?= $row["namakriteria"] ?>' id='<?= $row["namakriteria"] ?>' name='data[<?= $row["namakriteria"] ?>]' value="">
              <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
            </div>
          <?php } ?>
          <div class="form-group">
            <label >Pendidikan</label>
            <select disabled class="form-control" id='Pendidikan' name='data[Pendidikan]'>
              <option value="">Pilih Pendidikan</option>
              <option value="100">S1</option>
              <option value="100">S1 H</option>
              <option value="80">D3</option>
              <option value="80">D3 H</option>
              <option value="60">SMA / SMK</option>
              <option value="60">SMA H / SMK H</option>
            </select>
            <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<!-- End Modal -->

<?php
  if(isset($_GET["aksi"]))
  {
    $thun = $_GET["tahun"];
    $unit = $_GET["unit"];
    $result_m = mysqli_query($conn, "SELECT promosi.id_promosi, promosi.tanggal, unit.namaunit, jabatan.namajabatan FROM promosi INNER JOIN unit ON promosi.id_unit = unit.id_unit INNER JOIN jabatan ON promosi.id_jabatan = jabatan.id_jabatan WHERE  YEAR (promosi.tanggal) = '". $thun ."' AND promosi.id_unit = '". $unit ."'");
    $namaunit = mysqli_fetch_assoc($result_m)["namaunit"];
    mysqli_data_seek($result_m, 0);
  }
?>

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i>Laporan Promosi Pegawai</h3>
        <button class="btn btn-info pull-right" type="button" onClick="printdiv('printarea');">Cetak Laporan</button>
      </div> <br>

      <style type="text/css">
          .printMe {display: none;}
          @media print {
              div {display: none;}
              .header-print {display:block;}
              .printMe {display: block;}
          }
      </style>

      <script language="javascript">
      function printdiv(printpage)
      {
      var headstr = "<html><head><title></title></head><body>";
      var footstr = "</body>";
      var newstr = document.all.item(printpage).innerHTML;
      var oldstr = document.body.innerHTML;
      document.body.innerHTML = headstr+newstr+footstr;
      window.print();
      // setTimeout(function(){ window.print() }, 100);
      document.body.innerHTML = oldstr;
      return false;
      }
      </script>
      
      <div class="box-body table-responsive">
      <form action="" method="GET" enctype="multipart/form-data" name="frm_penilaian">
        
        <div class="form-group">
          <input type="hidden" name="page" value="laporan_promosi">
          <label for="tanggal" class="control-label">Tahun</label>
<!--           <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="<?= isset($_GET['tanggal']) ? $_GET['tanggal'] : ''?>"> -->
<input type="text" class="form-control" id='tahun' name='tahun' placeholder="Tahun" value='<?= isset($_GET["tahun"]) ? $_GET["tahun"] : "" ?>'></input>
        </div>
           <div class="form-group">
                <label >Unit</label>
                 <?php
                  echo "<select class='form-control' id='kopunit' name='unit'>";
                    $unit = mysqli_query($conn, "SELECT * FROM unit");
                  echo "<option value='' selected> - Pilih Unit - </option>";
                  while ($a = mysqli_fetch_assoc($unit)) {
                    echo "<option value='".$a['id_unit']."'>".$a['namaunit']."</option>";
                  }
              echo "</select>";

              $a = isset($_GET["tahun"]) ? $_GET["tahun"] : "" ;
              $b = isset($_GET["unit"]) ? $_GET["unit"] : "" ;
            ?>
        </div>
        <div style="clear:both;"></div>
        <button type="submit" class="btn btn-info pull-right" name="aksi" value="cari">Cari Data</button>
        <div style="clear:both;"></div><br />
<div id="printarea">   
          <div class="printMe">
            <script type="text/javascript">document.write('<center><h1><b>' + document.title  + '</b></h1></center>')</script>
            <?php 

            function random($length = 4) {
                $chars = '0123456789abc';

                $Code = '';
                for ($i = 0; $i < $length; ++$i) {
                    $Code .= substr($chars, (((int) mt_rand(0, strlen($chars))) - 1), 1);
                }
                return strtoupper($Code);
            }


            ?>
            <center>
            <h2><b>LAPORAN PROMOSI</b></h2> <br>
            <small>Nomor Surat : SK-LAP/LAP-<?php print_r(random(4))?>/v/<?php echo date("Y"); ?> </small> <br>
            <small>Dikeluarkan oleh : <?php echo $_SESSION['status'] ?> </small>
            </center>
            
            <br>

            <div class="pull-left">
              <h4> Tahun Promosi : <?php echo isset($_GET["tahun"]) ? $_GET["tahun"] : "" ?> </h4>
              <h4> Unit Tujuan : <?php echo isset($namaunit) ? $namaunit : "" ?> </h4>
            </div>  <br><br>


          </div>

        
    <?php if(isset($_GET["aksi"])) { ?>
    <br />
    <!-- <h3 style="text-align:center;margin:0px"><strong>Hasil Seleksi</strong><hr></h3> -->
    <?php
            while($rowm = mysqli_fetch_assoc($result_m)) {
              //echo $rowm["id_mutasi"];
    ?>

            <?php
              $no = 1;
              $datanilai = array();
              $ress = mysqli_query($conn, "SELECT p.nip, p.namapegawai, p.tanggallahir, p.jeniskelamin, u.namaunit, pen.namapendidikan FROM promosi_detail as md INNER JOIN pegawai as p ON md.id_pegawai = p.id_pegawai INNER JOIN unit as u ON p.unit = u.id_unit INNER JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan WHERE md.id_promosi = '". $rowm["id_promosi"] ."'");
              while($row = mysqli_fetch_assoc($ress)) {
                $datanilai[] = $row;
              }
            ?>

        <!-- Tabel Penilaian -->
        <h4 style="text-align:left">Hasil untuk jabatan : <?= $rowm["namajabatan"] ?></h4>
        <table id="tabel" class="table table-bordered table-hover">
          <?php 
            $res = mysqli_query($conn, "SELECT namakriteria FROM kriteria ORDER BY id_kriteria ASC");
            $jumlah = mysqli_num_rows($res);
          ?>
          <thead>
            <tr rowspan="2">
              <th rowspan="2" width="2%" valign="middle">No</th>
              <th rowspan="2" width="2%" valign="middle">NIP</th>
              <th rowspan="2" valign="middle">Nama</th>
              <th rowspan="2" valign="middle">Jabatan</th>
              <th rowspan="2" valign="middle">Unit</th>
              <th rowspan="2" width="10%" valign="middle">Pendidikan</th>
              <th rowspan="2" width="5%" class="text-center" valign="middle">Tahun</th>              
              <th rowspan="2" width="12%" class="text-center">Total Nilai</th>
             </tr>
          </thead>
          <tbody>
            
            <?php
                $result = mysqli_query($conn, "SELECT jabatan.namajabatan,u.namaunit,p.nip,pen.namapendidikan,p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit, pg.tahun from pegawai as p INNER JOIN promosi_detail as md ON p.id_pegawai = md.id_pegawai LEFT JOIN  penilaianpegawai as pg ON p.id_pegawai = pg.id_pegawai LEFT JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan LEFT JOIN unit as u ON p.unit = u.id_unit LEFT JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg INNER JOIN jabatan ON p.jabatan = jabatan.id_jabatan WHERE md.id_promosi ='". $rowm["id_promosi"]."' GROUP BY pg.id_pg, p.id_pegawai, pg.tahun ORDER BY total DESC");
                $i=1;
                while ($row = mysqli_fetch_assoc($result)) {
                    $r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd LEFT JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
                    echo "<tr>";
                    echo "<td align='center'>".$i."</td>
                    <td align='center'>".$row['nip']."</td>
                    <td id='nk".$row['id_pg']."'>".$row['namapegawai']."</td>

                    <td align='center'>".$row['namajabatan']."</td>
                    <td align='center'>".$row['namaunit']."</td>
                    <td align='center'>".$row['namapendidikan']."</td>" ;
                    echo empty($row["tahun"]) ? "<td class='text-center'> - </td>" : "<td class='text-center'>".$row['tahun']."</td>";
                    
                    echo empty($row["total"]) ? "<td class='text-center'>-</td>" : "<td align='center'><input type='hidden' name='id_pg' id='id_pg' class='id_pg' value='".$row["id_pg"]."'>".$row["total"]." &nbsp; <span data-toggle='modal' data-target='#myModal' class='hidden-print view-btn'><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Lihat Data'><i class='glyphicon glyphicon-eye-open'></i></a></span></td>";
                    echo "</tr>";
                    $i++;
                } 
            ?>
            <?php
                if(mysqli_num_rows($ress) < 1) {
            ?>
                <td colspan='<?= intval($jumlah + 4) ?>' class="text-center">Tidak ada yang memenuhi</td>
            <?php } ?>
          </tbody>
        </table>
        <?php   } 
              }
        ?>
</div>       
        
        </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<!-- session storage for selected dropdown -->
<script type="text/javascript">
  window.onload = function() {
    var selItem = sessionStorage.getItem("kopunit"); 
    var selItem2 = sessionStorage.getItem("projabatan") 
    $('#kopunit').val(selItem);
    $('#projabatan').val(selItem2);
    }
    $('#kopunit').change(function() { 
        var selVal = $(this).val();
        sessionStorage.setItem("kopunit", selVal);
    });

    $('#projabatan').change(function() { 
        var selVal2 = $(this).val();
        sessionStorage.setItem("projabatan", selVal2);
    });
</script>

<?php
  if(isset($_POST['simpan'])){
    $id_kriteria = $_POST['id_kriteria'];
    $namakriteria = $_POST['namakriteria'];

    if ($id_kriteria == "") {
      $query ="INSERT INTO kriteria (namakriteria) VALUES ('$namakriteria')";
      $info ="tambah";
    }
    elseif($id_kriteria != "") {
      $query = "update kriteria set namakriteria='$namakriteria' where id_kriteria='$id_kriteria'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from kriteria where id_kriteria='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
    namaunit: {required: true}, 
      
    },
    messages: {
    namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
  nk = $("#nk"+id).text();
 
  $("#namakriteria").val(nk);
    
  $("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
  
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

  $("#tahun").datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });
  
  $(".view-btn").on("click", function(e) {
      self = $(this);
      console.log(self);
      $parent = $(this).parents("td");
      $id_pg = $parent.find(".id_pg").val();
      $.post(base_url + "aksi/nilai_pegawai.php", {id_pg: $id_pg}, function(res){
        //console.log(res);
        $("#id_pg").val($id_pg);
        $.each(res, function(k, v) {
            //display the key and value pair
            // $()
            // $("input[name^=data["+ k +"]]").val(v);
            //console.log(v);
            $('input[name^="data['+k+']"]').val(v);
            $('select[name^="data['+k+']"]').val(v);
            //console.log($('input[name^="data['+k+']"]').html());
        });
      }, "json");
    })
</script>
