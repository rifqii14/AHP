<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form kriteria</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <input type="hidden" name="aksi-h" id="aksi-h" value="tambah" />
          <form action="" method="post" id="formiki" action="index.php?page=kriteria">
          
            <div class="form-group">
              <label >Nama Kriteria</label>
              <input type="text" class="form-control" placeholder="Nama Kriteria" id="namakriteria" name="namakriteria" value=""></input>
              <span style="color:red" id="err-kriteria" >Nama kriteria sudah digunakan. Mohon masukan Kriteria lain</span>
              <input type="text" id="id" name="id_kriteria" value="" hidden></input>
            </div>
            
           <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan" id="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Kriteria</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' id="tambah-p" style="margin: 10px"><i class="fa fa-plus"></i> Tambah Kriteria</button>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Nama Kriteria</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from kriteria");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
				          <td id='nk".$row['id_kriteria']."'>".$row['namakriteria']."</td>
                  <td align='center'>
                   <span class='edit-p' data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_kriteria'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_kriteria'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['namakriteria'])){
    $id_kriteria = $_POST['id_kriteria'];
    $namakriteria = $_POST['namakriteria'];

    if ($id_kriteria == "") {
      $query ="INSERT INTO kriteria (namakriteria) VALUES ('$namakriteria')";
      $info ="tambah";
    }
    elseif($id_kriteria != "") {
      $query = "update kriteria set namakriteria='$namakriteria' where id_kriteria='$id_kriteria'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from kriteria where id_kriteria='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
 $("#err-kriteria").hide();
  $('#tabel').dataTable();

  $(".alert" ).fadeOut(8000);

  $('#simpan').on('click', function(e) {
  
  var validator = $("#formiki").validate({
    rules: {
      namakriteria: {required: true}
    },
    messages: {
      namakriteria: {required: "Nama Kriteria tidak boleh kosong"}
    }
  }); 

  if($("#formiki").valid()) {
    var self = $(this).parents("#formiki");
    $kriteria = self.find("#namakriteria").val();
    //console.log ($nip);
    $aksiform = $("#aksi-h").val();
    if($aksiform === "add") {
    $.post(base_url + "aksi/cek_kriteria.php", {namakriteria : $kriteria}, function(res){
      if(res !== "ada"){
        $("#err-kriteria").hide();
        console.log(res);
        document.getElementById("formiki").submit();
      } else {
        console.log(res);
        $("#err-kriteria").show();
        
      }
    }, "json");
    
    }else {
      document.getElementById("formiki").submit();
    }
    e.preventDefault();
  }
});


  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

  $("#tambah-p").on("click", function() {
  $("#aksi-h").val("add");
});

$("#tabel").on("click", ".edit-p", function() {
  $("#aksi-h").val("edit");
  $("#err-kriteria").hide();
});
</script>
