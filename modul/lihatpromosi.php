
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- Modal -->
<div class="modal fade " role="dialog" id="myModal">
  <div class=" modal-dialog">
    <div class="box box-solid box-primary modal-content">

      <div class="box-header">
        <h3 class="box-title"><i class="ion-person-add"></i> &nbsp; kriteria</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
        </div>
      </div>

      <div class="box-body">
        <form action="" method="post" id="formiki">
          <input type="hidden" id="id_pg" name="id_pg" value="">
            <?php
              $query = "SELECT * FROM kriteria WHERE NOT namakriteria = 'Pendidikan'";
              $data = mysqli_query($conn, $query);
              while ($row = mysqli_fetch_assoc($data)) {
            ?>
          
            <div class="form-group">
              <label ><?= $row["namakriteria"] ?></label>
              <input disabled type="number" min="0" class="form-control" placeholder='<?= $row["namakriteria"] ?>' id='<?= $row["namakriteria"] ?>' name='data[<?= $row["namakriteria"] ?>]' value="">
              <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
            </div>
          <?php } ?>
          <div class="form-group">
            <label >Pendidikan</label>
            <select disabled class="form-control" id='Pendidikan' name='data[Pendidikan]'>
              <option value="">Pilih Pendidikan</option>
              <option value="100">S1</option>
              <option value="100">S1 H</option>
              <option value="80">D3</option>
              <option value="80">D3 H</option>
              <option value="60">SMA / SMK</option>
              <option value="60">SMA H / SMK H</option>
            </select>
            <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<!-- End Modal -->
<?php
  if(isset($_GET["aksi"]))
  {
    //   $jabatan = $_GET["jabatan"];
    //   $unit = $_GET["unit"];

    //   $res = mysqli_query($conn, "SELECT is_khusus FROM unit WHERE id_unit ='". $unit ."'");
    //   $is_khusus = (bool)mysqli_fetch_assoc($res)["is_khusus"];

    //   if($is_khusus) 
    //   {
    //     $res = mysqli_query($conn, "SELECT jabatan.namajabatan, pendidikan.id_pendidikan, pendidikan.tingkatan FROM pendidikan_jabatan INNER JOIN jabatan ON pendidikan_jabatan.id_jabatan = jabatan.id_jabatan INNER JOIN pendidikan ON pendidikan_jabatan.id_pendidikan = pendidikan.id_pendidikan WHERE pendidikan.namapendidikan LIKE '%H' AND pendidikan_jabatan.id_jabatan = '". $jabatan ."'");
    //     $tingkatan = mysqli_fetch_assoc($res)["tingkatan"];

    //     $ress = mysqli_query($conn, "SELECT pegawai.id_pegawai, pegawai.nip, pegawai.namapegawai, pendidikan.namapendidikan, pegawai.jeniskelamin, pegawai.tanggallahir, unit.namaunit FROM pegawai INNER JOIN unit ON pegawai.unit = unit.id_unit INNER JOIN pendidikan on pegawai.pendidikan = pendidikan.id_pendidikan WHERE pendidikan.tingkatan >= '". $tingkatan ."' AND pendidikan.namapendidikan LIKE '%H' ORDER BY pendidikan.tingkatan DESC");
    //   }
    //   else
    //   {
    //     $res = mysqli_query($conn, "SELECT jabatan.namajabatan, pendidikan.id_pendidikan, pendidikan.tingkatan FROM pendidikan_jabatan INNER JOIN jabatan ON pendidikan_jabatan.id_jabatan = jabatan.id_jabatan INNER JOIN pendidikan ON pendidikan_jabatan.id_pendidikan = pendidikan.id_pendidikan WHERE NOT pendidikan.namapendidikan LIKE '%H' AND pendidikan_jabatan.id_jabatan = '". $jabatan ."'");
    //     $tingkatan = mysqli_fetch_assoc($res)["tingkatan"];

    //     $ress = mysqli_query($conn, "SELECT pegawai.id_pegawai, pegawai.nip, pegawai.namapegawai, pendidikan.namapendidikan, pegawai.jeniskelamin, pegawai.tanggallahir, unit.namaunit FROM pegawai INNER JOIN unit ON pegawai.unit = unit.id_unit INNER JOIN pendidikan on pegawai.pendidikan = pendidikan.id_pendidikan WHERE pendidikan.tingkatan >= '". $tingkatan ."' AND NOT pendidikan.namapendidikan LIKE '%H' ORDER BY pendidikan.tingkatan DESC");
    //   }
      
  }

  $id = $_GET["id"];
  $result = mysqli_query($conn, "SELECT promosi.id_promosi, promosi.tanggal, unit.namaunit, jabatan.namajabatan FROM promosi INNER JOIN unit ON promosi.id_unit = unit.id_unit INNER JOIN jabatan ON promosi.id_jabatan = jabatan.id_jabatan WHERE id_promosi ='". $id ."'");
  $data = mysqli_fetch_assoc($result);
?>
<!-- tambah user -->

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> 
            <a href="?page=promosi" class="btn btn-info">Kembali</a>
            Data Promosi
        </h3>
      </div>
      
      <div class="box-body table-responsive">
      
      <form action="" method="POST" enctype="multipart/form-data" name="frm_promosi" class="form-horizontal">
        <div class="form-group">
          <label for="tanggal" class="col-sm-1 control-label">Tanggal</label>
          <div class="col-sm-11">
            <input disabled type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="<?= $data['tanggal'] ?>">
          </div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail3" class="col-sm-1 control-label">Unit</label>
            <div class="col-sm-11">
                <select class="form-control" name="unit" id="unit" required disabled>
                <option value="">Unit</option>
                <?php
                    $r = mysqli_query($conn, "SELECT id_unit, namaunit FROM unit");
                    while($row = mysqli_fetch_assoc($r)) {
                        $uselected = $data["namaunit"] == $row["namaunit"] ? "selected" : "";
                ?>
                    <option value='<?= $row["id_unit"]?>' <?= $uselected ?> ><?= $row["namaunit"]?></option>
                <?php } ?>
                
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail3" class="col-sm-1 control-label">Jabatan Promosi</label>
            <div class="col-sm-11">
                <select class="form-control" name="jabatan" id="jabatan" required disabled>
                    <option value="">Jabatan promosi</option>
                    <?php
                    $r2 = mysqli_query($conn, "SELECT id_jabatan, namajabatan FROM jabatan");
                    while($row = mysqli_fetch_assoc($r2)) {
                        $jselected = $data["namajabatan"] == $row["namajabatan"] ? "selected" : "";
                    ?>
                    <option value='<?= $row["id_jabatan"]?>' <?= $jselected ?> ><?= $row["namajabatan"]?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
      

      
        <br/>
      
            <?php
              $no = 1;
              $datanilai = array();
              $ress = mysqli_query($conn, "SELECT p.nip, p.namapegawai, p.tanggallahir, p.jeniskelamin, u.namaunit, pen.namapendidikan FROM promosi_detail as md INNER JOIN pegawai as p ON md.id_pegawai = p.id_pegawai INNER JOIN unit as u ON p.unit = u.id_unit INNER JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan WHERE md.id_promosi = '". $id ."'");
              while($row = mysqli_fetch_assoc($ress)) {
                $datanilai[] = $row;
                }
            ?>

        <!-- Tabel Penilaian -->
        <h4 class="header">Hasil Seleksi<hr/></h4>
        <table id="tabel" class="table table-bordered table-hover">
          <?php 
            $res = mysqli_query($conn, "SELECT namakriteria FROM kriteria ORDER BY id_kriteria ASC");
            $jumlah = mysqli_num_rows($res);
          ?>
          <thead>
            <tr rowspan="2">
              <th rowspan="2" width="2%" valign="middle">Urutan</th>
              <th rowspan="2" width="2%" valign="middle">NIP</th>
              <th rowspan="2" valign="middle">Nama</th>
              <th rowspan="2" valign="middle">Jabatan</th>
              <th rowspan="2" valign="middle">Unit</th>
              <th rowspan="2" width="10%" valign="middle">Pendidikan</th>
              <th rowspan="2" width="5%" valign="middle">Tahun</th>
              <th rowspan="2" width="12%" class="text-center">Total Nilai</th>

          </thead>
          <tbody>
            
            <?php
                $result = mysqli_query($conn, "SELECT jabatan.namajabatan,u.namaunit,p.nip,pen.namapendidikan,p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit, pg.tahun from pegawai as p INNER JOIN promosi_detail as md ON p.id_pegawai = md.id_pegawai LEFT JOIN  penilaianpegawai as pg ON p.id_pegawai = pg.id_pegawai LEFT JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan LEFT JOIN unit as u ON p.unit = u.id_unit LEFT JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg  INNER JOIN jabatan ON p.jabatan = jabatan.id_jabatan WHERE md.id_promosi >='". $id."' GROUP BY pg.id_pg, p.id_pegawai, pg.tahun ORDER BY total DESC");
                $i=1;
                while ($row = mysqli_fetch_assoc($result)) {
                    $r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd LEFT JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
                    echo "<tr>";
                    echo "<td align='center'>".$i."</td>
                    <td align='center'>".$row['nip']."</td>
                    <td id='nk".$row['id_pg']."'>".$row['namapegawai']."</td>
                    <td align='center'>".$row['namajabatan']."</td>
                    <td align='center'>".$row['namaunit']."</td>
                    <td align='center'>".$row['namapendidikan']."</td>";
                    echo empty($row["tahun"]) ? "<td class='text-center'> - </td>" : "<td class='text-center'>".$row['tahun']."</td>";
                    
                    echo empty($row["total"]) ? "<td class='text-center'>-</td>" : "<td align='center'><input type='hidden' name='id_pg' id='id_pg' class='id_pg' value='".$row["id_pg"]."'>".$row["total"]." &nbsp; <span data-toggle='modal' data-target='#myModal' class='hidden-print view-btn'><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Lihat Data'><i class='glyphicon glyphicon-eye-open'></i></a></span></td>";
                    echo "</tr>";
                    $i++;
                } 
            ?>
            <?php
                if(mysqli_num_rows($ress) < 1) {
            ?>
                <td colspan='<?= intval($jumlah + 4) ?>' class="text-center">Tidak ada yang memenuhi</td>
            <?php } ?>
          </tbody>
        </table>
      </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->


<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

 $("#tanggal").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        weekStart: 1,
        todayBtn:  1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

$(".view-btn").on("click", function(e) {
  self = $(this);
  console.log(self);
  $parent = $(this).parents("td");
  $id_pg = $parent.find(".id_pg").val();
  $.post(base_url + "aksi/nilai_pegawai.php", {id_pg: $id_pg}, function(res){
    //console.log(res);
    $("#id_pg").val($id_pg);
    $.each(res, function(k, v) {
        //display the key and value pair
        // $()
        // $("input[name^=data["+ k +"]]").val(v);
        //console.log(v);
        $('input[name^="data['+k+']"]').val(v);
        $('select[name^="data['+k+']"]').val(v);
        //console.log($('input[name^="data['+k+']"]').html());
    });
  }, "json");
})
</script>
