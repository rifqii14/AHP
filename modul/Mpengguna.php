<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>


<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box">

      <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fw fa-user"></i>List User</h3>
        <div class="box-tools pull-right">
          <button class="btn" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>

      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Status</th>  
            </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from user order by nama asc");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
                  <td id='nm".$row['id_user']."'>".$row['nama']."</td>
                  <td id='un".$row['id_user']."'>".$row['username']."</td>
                  <td id='st".$row['id_user']."'>".$row['status']."</td>   
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['kirim'])){
    $id_user = $_POST['id_user'];
    $nama = $_POST['nama'];
    $username = $_POST['username'];
    $status = $_POST['status'];
    $password = md5($_POST['password']);

    if ($id_user == "") {
      $query ="INSERT INTO user (nama, username, password, email, status) VALUES ('$nama', '$username', '$password', '$status')";
      $info ="tambah";
    }
    elseif($id_user != "" and $password =="") {
      $query = "update user set nama='$nama', username='$username', status='$status' where id_user='$id_user'";
      $info ="update";
    }
    elseif ($id_user != "" and $password != "") {
      $query = "update user set nama='$nama', username='$username',  status='$status', password='$password' where id_user='$id_user'";
      $info ="update";
    }
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mpengguna&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from user where id_user='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mpengguna&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
      nama: {required: true},
      username: {required: true},
      password: {required: true, minlength:6},
      status: {required: true},
    },
    messages: {
      nama: {required: "Nama tidak boleh kosong"},
      username: {required: "Username tidak boleh kosong"},
      password: {required: "Password tidak boleh kosong", minlength:"Minimal 6 digit"},
      status: {required: "Status tidak boleh kosong"},
    }
  });
  function edit(id){
    nm = $("#nm"+id).text();
    un = $("#un"+id).text();
    st = $("#st"+id).text();

    $("#nama").val(nm);
    $("#username").val(un);
    $("#status").val(st).change();
    $("#id").val(id);
  }

  function hapus(id){
   document.location.href = base_url+'index.php?page=Mpengguna&hapus='+id;
  }
</script>
