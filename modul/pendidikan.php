<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<linK href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form pendidikan</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <input type="hidden" name="aksi-h" id="aksi-h" value="tambah" />
          <form method="POST" id="formiki" action="index.php?page=pendidikan" >
          
            <div class="form-group">
              <label >Nama pendidikan</label>
              <input type="text" class="form-control" placeholder="Nama pendidikan" id="namapendidikan" name="namapendidikan" value=""></input>
              <span style="color:red" id="err-pen" >Nama Sudah digunakan. Mohon gunakan nama lain</span>
              <input type="text" id="id" name="id_pendidikan" value="" hidden></input>
            </div>
            <div class="form-group">
              <label >Tingkatan</label>
              <input type="number" min="0" class="form-control" placeholder="Tingkatan" id="tingkatan" name="tingkatan" value=""></input>
            </div>
            
           <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan" id="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar pendidikan</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' style="margin: 10px" id="tambah-p"><i class="fa fa-plus"></i> Tambah pendidikan</button>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Nama pendidikan</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from pendidikan");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
				          <td id='np".$row['id_pendidikan']."'>".$row['namapendidikan']."</td>
                  <td align='center'>
                  <input type='hidden' id='tp". $row['id_pendidikan']."' value='". $row["tingkatan"]. "'>
                   <span class='edit-p' data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_pendidikan'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_pendidikan'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['namapendidikan'])){
    $id_pendidikan = $_POST['id_pendidikan'];
    $namapendidikan = $_POST['namapendidikan'];
    $tingkatan = $_POST["tingkatan"];

    if ($id_pendidikan == "") {
      $query ="INSERT INTO pendidikan (namapendidikan, tingkatan) VALUES ('$namapendidikan','$tingkatan')";
      $info ="tambah";
    }
    elseif($id_pendidikan != "") {
      $query = "update pendidikan set namapendidikan='$namapendidikan', tingkatan = '$tingkatan' where id_pendidikan='$id_pendidikan'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=pendidikan&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from pendidikan where id_pendidikan='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=pendidikan&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $("#err-pen").hide();
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  
  function edit(id){
	np = $("#np"+id).text();
  tp = $("#tp"+id).val();
	$("#namapendidikan").val(np);
  $("#tingkatan").val(tp);  
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=pendidikan&hapus='+id;
  }

  $('#simpan').on('click', function(e) {
  
    var validator = $("#formiki").validate({
      rules: {
      namapendidikan: {required: true},	
        
      },
      messages: {
      namapendidikan: {required: "Nama pendidikan tidak boleh kosong"},
        
      }
    });

  if($("#formiki").valid()) {
    var self = $(this).parents("#formiki");
    $namapendidikan = self.find("#namapendidikan").val();
    //console.log ($namapendidikan);
    $aksiform = $("#aksi-h").val();
    if($aksiform === "add") {
    $.post(base_url + "aksi/cek_pendidikan.php", {namapendidikan : $namapendidikan}, function(res){
      if(res !== "ada"){
        $("#err-pen").hide();
        //console.log(res);
        document.getElementById("formiki").submit();
      } else {
        //console.log(res);
        $("#err-pen").show();
        
      }
    }, "json");
    
    }else {
      document.getElementById("formiki").submit();
    }
    e.preventDefault();
  }
});

  $("#tambah-p").on("click", function() {
    $("#aksi-h").val("add");
  });
  $("#tabel").on("click", ".edit-p", function() {
    $("#aksi-h").val("edit");
    $("#err-pen").hide();
  });
</script>
