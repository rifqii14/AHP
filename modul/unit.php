<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form Unit</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <input type="hidden" name="aksi-h" id="aksi-h" value="tambah" />
          <form action="" method="post" id="formiki"  action="index.php?page=unit">
          
            <div class="form-group">
              <label >Nama Unit</label>
              <input type="text" class="form-control" placeholder="Nama Unit" id="namaunit" name="namaunit" value=""></input>
              <span style="color:red" id="err-unit" >Nama unit sudah digunakan. Mohon masukan Unit lain</span>
              <input type="text" id="id" name="id_unit" value="" hidden></input>
            </div>
            <div class="checkbox">
              
              
              <label ><input type="checkbox" id="iskhusus" name="iskhusus" value=""></input> Butuh Pendidikan Khusus?</label>
            </div>
        
            <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan" id="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Unit</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' id="tambah-p" style="margin: 10px"><i class="fa fa-plus"></i> Tambah Unit</button>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Nama Unit</th>
              <th width="15%">Pendidikan Khusus</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from unit");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                $khusus = $row["is_khusus"] == 1 ? "ya" : "tidak";
                 echo "<tr>
                  <td align='center'>".$i."</td>
				  <td id='nu".$row['id_unit']."'>".$row['namaunit']."</td>
          <td id='nkh".$row['id_unit']."'>".$khusus."</td>
                  <td align='center'>
                   <span class='edit-p' data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_unit'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_unit'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['namaunit'])){
    $id_unit = $_POST['id_unit'];
    $namaunit = $_POST['namaunit'];	  
    $is_khusus = isset($_POST["iskhusus"]);

    if ($id_unit == "") {
      $query ="INSERT INTO unit (namaunit, is_khusus) VALUES ('$namaunit', '$is_khusus')";
      $info ="tambah";
    }
    elseif($id_unit != "") {
      $query = "update unit set namaunit='$namaunit', is_khusus = '$is_khusus' where id_unit='$id_unit'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=unit&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from unit where id_unit='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=unit&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#err-unit').hide();
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);


  $('#simpan').on('click', function(e) {
  
  var validator = $("#formiki").validate({
    rules:{
    namaunit: {required: true}
      
    },
    messages: {
    namaunit: {required: "Nama Unit tidak boleh kosong"}
    }
  }); 

  if($("#formiki").valid()) {
    var self = $(this).parents("#formiki");
    $unit = self.find("#namaunit").val();
    //console.log ($nip);
    $aksiform = $("#aksi-h").val();
    if($aksiform === "add") {
    $.post(base_url + "aksi/cek_unit.php", {namaunit : $unit}, function(res){
      if(res !== "ada"){
        $("#err-unit").hide();
        console.log(res);
        document.getElementById("formiki").submit();
      } else {
        console.log(res);
        $("#err-unit").show();
        
      }
    }, "json");
    
    }else {
      document.getElementById("formiki").submit();
    }
    e.preventDefault();
  }
});

  function edit(id){
	nu = $("#nu"+id).text();
  var khusus = $("#nkh" + id).text() === "ya" ? true : false;
  console.log(khusus);
  if(khusus)
  {
    $('#iskhusus').iCheck('check');
  }else{
    $('#iskhusus').iCheck('uncheck');
  }
	$("#namaunit").val(nu);
  //$("#iskhusus").prop("checked", khusus);  
	$("#id").val(id);
  }

  function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }

  function hapus(id){
   document.location.href = base_url+'index.php?page=unit&hapus='+id;
  }

$('.datepicker').datetimepicker({
  language:  'id',
  weekStart: 1,
  todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
});

  $("#tambah-p").on("click", function() {
  $("#aksi-h").val("add");
});

$("#tabel").on("click", ".edit-p", function() {
  $("#aksi-h").val("edit");
  $("#err-unit").hide();
});
</script>
