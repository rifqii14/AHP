
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<?php
  if(isset($_POST["save"]))
  {
      $action_result = save_nilai_pegawai($conn);
      if($action_result)
      {
        $info ="tambah";
        echo "<script> document.location.href = base_url+'index.php?page=penilaianpegawai&info=".$info."'</script>";
      }
  }

//   $id = $_GET["id"];
//   $result = mysqli_query($conn, "SELECT promosi.id_promosi, promosi.tanggal, unit.namaunit, jabatan.namajabatan FROM promosi INNER JOIN unit ON promosi.id_unit = unit.id_unit INNER JOIN jabatan ON promosi.id_jabatan = jabatan.id_jabatan WHERE id_promosi ='". $id ."'");
//   $data = mysqli_fetch_assoc($result);
?>
<!-- tambah user -->

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> 
            <a href="?page=penilaianpegawai" class="btn btn-info">Kembali</a>
            Penilaian Pegawai
        </h3>
      </div>
      
      <div class="box-body table-responsive">
      <form action="" method="GET" enctype="multipart/form-data" name="" id="" class="form-horizontal">
        <div class="form-group">
          <label for="tahun" class="col-sm-1 control-label">Tahun</label>
          <div class="col-sm-11">
            <input type="hidden" value="tambah_pg" name="page" />
            <input type="text" class="form-control" id="tahun" name="tahun" placeholder="tahun" value='<?= isset($_GET["tahun"]) ? $_GET["tahun"] : date("Y") ?>'>
          </div>
        </div>
        <div class="form-group">
          <label for="tahun" class="col-sm-1 control-label sr-only">Tahun</label>
          <div class="col-sm-11">
            <button type="submit" class="btn btn-info">Cek Data</button>
          </div>
        </div>
      </form>
      <form action="" method="POST" enctype="multipart/form-data" name="frm_pg" id="frm_pg" class="form-horizontal">
        <input type="hidden" name="tahun" value='<?= isset($_GET["tahun"]) ? $_GET["tahun"] : date("Y") ?>'>
        <br>
        <!-- Tabel Penilaian -->
        <h4 class="header">Daftar Pegawai<hr/></h4>
        <table id="tabel" class="table table-bordered table-hover">
          <?php 
            $res = mysqli_query($conn, "SELECT id_kriteria, namakriteria FROM kriteria ORDER BY id_kriteria ASC");
            $jumlah = mysqli_num_rows($res);
          ?>
          <thead>
            <tr rowspan="2">
              <th rowspan="2" width="2%" valign="middle">Urutan</th>
              <th rowspan="2" width="20%" valign="middle">Nama Pegawai</th>
              <th colspan='<?= mysqli_num_rows($res)?>' class="text-center" >Nilai Kriteria</th>
              
             </tr>
             <tr>
                <?php
                    while($row = mysqli_fetch_assoc($res)) {
                ?>
                        <th><?= $row["namakriteria"]?></th>
                    <?php } ?>
             </tr>
          </thead>
          <tbody>
            
            <?php
                $dt_thn = isset($_GET["tahun"]) ? $_GET["tahun"] : date("Y");
                $result = mysqli_query($conn, "SELECT p.id_pegawai, p.namapegawai FROM pegawai as p INNER JOIN unit as u ON p.unit = u.id_unit WHERE u.namaunit = '". $_SESSION["namaunit"] ."' AND p.id_pegawai NOT IN (SELECT pg.id_pegawai FROM penilaianpegawai as pg WHERE tahun = '". $dt_thn ."')");
                $i=1;
                $nodata = false;
                if(mysqli_num_rows($result) < 1) {
                  $nodata = true;
            ?>
              <tr>
                <td colspan="<?= $jumlah + 2 ?>" class="text-center"> Tidak ada pegawai yang dapat dinilai </td>
              </tr>
                <?php
                }
                while ($row = mysqli_fetch_assoc($result)) {
                    //$r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd LEFT JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
                    echo "<tr>";
                    echo "<td align='center'>".$i."</td>
                    <td id='nk".$row['id_pegawai']."'>".$row['namapegawai']."</td>";

                    mysqli_data_seek($res, 0);
                    while($kri = mysqli_fetch_assoc($res)) {
                        if($kri["namakriteria"] !== "Pendidikan")
                        {
                        echo "<td><input type='text' class='form-control inp-pg' name=data[".$row['id_pegawai']."][".$kri['id_kriteria']."]></td>";
                        } else
                        {
                            echo '<td><select class="form-control" id="Pendidikan" name=data['.$row['id_pegawai'].']['.$kri['id_kriteria'].']>
                                <option value="">Pilih Pendidikan</option>
                                <option value="100">S1</option>
                                <option value="100">S1 H</option>
                                <option value="80">D3</option>
                                <option value="80">D3 H</option>
                                <option value="60">SMA / SMK</option>
                                <option value="60">SMA H / SMK H</option>
                            </select></td>';
                        }
                    }
                    
                    echo "</tr>";
                    $i++;
                } 
            ?>
          </tbody>
        </table>
        <br>
        <button type="submit" name="save" <?= $nodata ? "disabled" : "" ?> id="savebtn" class="btn btn-info btn-lg pull-right">Simpan Data</button>
        <div style="clear:both;"></div>
      </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->


<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

  $("#tahun").datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        endDate: '+0y',
        autoclose: true,
  });
  $("#savebtn").on("click", function(e) {
    //e.preventDefault();
    $pass = true;
    $(".inp-pg").each(function(i, el){
      $nilai = parseInt($(this).val());
      if($nilai > 100 || !$nilai){
        alert("Nilai tidak boleh lebih dari seratus");
        $(this).focus();
        $pass = false;
        return false;
      }
    });

    // if($pass)
    //   $("#frm_pg").submit();
    return $pass;
  });
  
</script>
