<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>


<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box">

      <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fw fa-user"></i>List Pegawai</h3>
        <div class="box-tools pull-right">
          <button class="btn" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>

      <div class="box-body table-responsive">

      <form name="cari" method="POST" action="">
           <div class="form-group">
                <label >BERDASARKAN UNIT</label>
                 <?php
                  echo "<select class='form-control' id='unit' name='unit'>";
                    $unit = mysqli_query($conn, "SELECT * FROM unit");
                  echo "<option value=''> Semua </option>";
                  while ($a = mysqli_fetch_assoc($unit)) {
                    echo "<option value='".$a['namaunit']."'>".$a['namaunit']."</option>";
                  }
              echo "</select>";
            ?>
        </div>
      
      <div style="clear:both;"></div>
          <button name="btncari" class="btn btn-info pull-right" type="submit">Cari Data</button> 
      <div style="clear:both;"></div>

      </form> 

        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>NIP</th>
              <th>Nama Pegawai</th>
              <th>Tempat Lahir</th>
              <th>Tanggal Lahir</th>
              <th width="">Alamat</th>
              <th width="13%">Jenis Kelamin</th>
              <th width="5%">Golongan Darah</th>
              <th>Pendidikan</th>
              <th width="14%">Jabatan</th>
              <th width="14%">Unit</th>
              
             </tr>
          </thead>
          <tbody>
            <?php

              if(isset($_POST['btncari'])){
              $result = mysqli_query($conn, "SELECT * from pegawai 
                inner join pendidikan on pendidikan.id_pendidikan = pegawai.pendidikan 
                inner join jabatan on jabatan.id_jabatan = pegawai.jabatan
                inner join unit on unit.id_unit = pegawai.unit WHERE namaunit LIKE '%$_POST[unit]%' order by namapegawai asc");
               }else{
                $result = mysqli_query($conn, "SELECT * from pegawai
                  inner join pendidikan on pendidikan.id_pendidikan = pegawai.pendidikan 
                  inner join jabatan on jabatan.id_jabatan = pegawai.jabatan
                  inner join unit on unit.id_unit = pegawai.unit order by namapegawai asc");
               }

              $jumlah = mysqli_num_rows($result);

              if($jumlah > 0) {

                      $i=1;
                      while ($row = mysqli_fetch_assoc($result)) {
                         echo "<tr>
                          <td align='center'>".$i."</td>
                          <td>".$row['nip']."</td>
                          <td>".$row['namapegawai']."</td>
                          <td>".$row['tempatlahir']."</td>
                          <td>".tanggal_format_indonesia($row['tanggallahir'])."</td>
                          <td>".$row['alamat']."</td>
                          <td>".$row['jeniskelamin']."</td>
                          <td>".$row['goldarah']."</td>
                          <td>".$row['namapendidikan']."</td>
                          <td>".$row['namajabatan']."</td>
                          <td>".$row['namaunit']."</td>
                        </tr>";
                        $i++;
                      }

              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>


<!-- session storage for selected dropdown -->
<script type="text/javascript">
  window.onload = function() {
    var selItem = sessionStorage.getItem("unit");  
    $('#unit').val(selItem);
    }
    $('#unit').change(function() { 
        var selVal = $(this).val();
        sessionStorage.setItem("unit", selVal);
    });
</script>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['kirim'])){
    $id_pegawai = $_POST['id_pegawai'];
    $namapegawai = $_POST['namapegawai'];
    $tempatlahir = $_POST['tempatlahir'];
    $tanggallahir = $_POST['tanggallahir'];
    $alamat = $_POST['alamat'];
	$jeniskelamin = $_POST['jeniskelamin'];
    $goldarah = $_POST['goldarah'];
	$pendidikan = $_POST['pendidikan'];
	$jabatan = $_POST['jabatan'];
    $unit = $_POST['unit'];

    if ($id_pegawai == "") {
       $query ="INSERT INTO pegawai (nip, namapegawai, tempatlahir, tanggallahir, alamat, jen_kelamin , goldarah,pendidikan, jabatan, unit) VALUES ('$nip','$namapegawai','$tempatlahir','$tanggallahir','$alamat','$jeniskelamin','$goldarah','$pendidikan','$jabatan','$unit')";
      $info ="tambah";
    }
    elseif($id_pegawai != "") {
     $query = "update pegawai set nip='$nip', namapegawai='$namapegawai', tempatlahir='$tempatlahir', tanggallahir='$tanggallahir', alamat='$alamat' ,jen_kelamin='$jeniskelamin' ,goldarah='$goldarah',pendidikan='$pendidikan',jabatan='$jabatan' unit='$unit' where id_pegawai='$id_pegawai'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mpegawai&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from pegawai where id_pegawai='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mpegawai&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
      namapegawai: {required: true},
      tempatlahir: {required: true},
      tanggallahir: {required: true},
      alamat: {required: true},
	  jeniskelamin: {required: true},
      goldarah: {required: true},
	  pendidikan: {required: true},
	  jabatan: {required: true},
	  unit:{required: true},
    },
    messages: {
      namapegawai: {required: "Nama tidak boleh kosong"},
      tempatlahir: {required: "Tempat lahir tidak boleh kosong"},
      tanggallahir: {required: "Tanggal lahir tidak boleh kosong"},
      alamat: {required: "alamat tidak boleh kosong"},
	  jeniskelamin: {required: "Jenis Kelamin tidak boleh kosong"},
      goldarah: {required: "Golongan Darah tidak boleh kosong"},
	  pendidikan: {required: "Pendidikan tidak boleh kosong"},
	  jabatan: {required: "Jabatan tidak boleh kosong"},
	  unit: {required: "Unit tidak boleh kosong"},
    }
  });
  function edit(id){
    nm = $("#nm"+id).text();
    tpl = $("#tpl"+id).text();
    tgl = $("#tgl"+id).text();
    al = $("#al"+id).text();
	jk = $("#jk"+id).text();
	gd = $("#gd"+id).text();
	pd = $("#pd"+id).text();
	jb = $("#jb"+id).text();
	un = $("#un"+id).text();
	  
    $("#nama").val(nm);
    $("#tempatlahir").val(un);
    $("#tanggallahir").val(un);
    $("#alamat").val(al);
	$("#jeniskelamin").val(jk).change();
    $("#goldarah").val(gd);
	$("#pendidikan").val(pd);
	$("#jabatan").val(jb).change();
	  $("#unit").val(un).change();
	$("#id").val(id);
  }
  function hapus(id){
  document.location.href = base_url+'index.php?page=Mpegawai&hapus='+id;
  }


</script>
