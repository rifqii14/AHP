<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Mutasi</h3>
      </div> <br>
      <a href="?page=tambahmutasi" class="btn btn-primary" style="margin: 10px"><i class="fa fa-plus"></i> Tambah Data Mutasi</a>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Tanggal</th>
              <th>Unit Tujuan</th>
              <th>Jabatan Mutasi</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "SELECT mutasi.id_mutasi, mutasi.tanggal, unit.namaunit, jabatan.namajabatan FROM mutasi INNER JOIN unit ON mutasi.id_unit = unit.id_unit INNER JOIN jabatan ON mutasi.id_jabatan = jabatan.id_jabatan WHERE namaunit = '".$_SESSION['namaunit']."'");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
            ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $row["tanggal"]?></td>
                    <td><?= $row["namaunit"]?></td>
                    <td><?= $row["namajabatan"]?></td>
                    <td align='center'>
                        <span ><a href="?page=lihatmutasi&id=<?= $row['id_mutasi'] ?>" class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Lihat Data'><i class='glyphicon glyphicon-eye-open'></i> </a></span> &nbsp;
                        <span class='btn btn-danger btn-sm' onclick='hapus(<?= $row['id_mutasi'] ?>)' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>  
                    </td>
                </tr>
            <?php } ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['simpan'])){
    // $id_kriteria = $_POST['id_kriteria'];
    // $namakriteria = $_POST['namakriteria'];

    // if ($id_kriteria == "") {
    //   $query ="INSERT INTO kriteria (namakriteria) VALUES ('$namakriteria')";
    //   $info ="tambah";
    // }
    // elseif($id_kriteria != "") {
    //   $query = "update kriteria set namakriteria='$namakriteria' where id_kriteria='$id_kriteria'";
    //   $info ="update";
    // }
    
    // $result = mysqli_query($conn, $query);
    // // print_r($result);die();
    // echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from mutasi_detail where id_mutasi='$id'";
    $result = mysqli_query($conn, $query);

    $query = "delete from mutasi where id_mutasi='$id'";
    $result2 = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=mutasi&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=mutasi&hapus='+id;
  }
</script>
