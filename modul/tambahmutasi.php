
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<?php
  if(isset($_POST["save"]))
  {
    $tanggal = date("Y-m-d");
    $id_jabatan = $_POST["id_jabatan"];
    $id_unit = $_POST["id_unit"];
    $dataIns = $_POST["data"];

    $rIns = mysqli_query($conn, "INSERT INTO mutasi (tanggal, id_unit, id_jabatan) VALUES ('". $tanggal ."', '". $id_unit ."', '". $id_jabatan ."')");
    $lastid = mysqli_insert_id($conn);

    foreach($dataIns as $key => $val)
    {
      $rIns2 = mysqli_query($conn, "INSERT INTO mutasi_detail (id_mutasi, id_pegawai) VALUES ('". $lastid ."', '". $val ."')");
    }
    // echo "<pre>";
    // print_r($dataIns);
    // echo "</pre>";
    echo "<script> document.location.href = base_url+'index.php?page=mutasi&info=tambah'</script>";
  }

?>
<?php
  if(isset($_GET["aksi"]))
  {
      $jabatan = $_GET["jabatan"];
      $unit = $_GET["unit"];

      $res = mysqli_query($conn, "SELECT is_khusus FROM unit WHERE id_unit ='". $unit ."'");
      $is_khusus = (bool)mysqli_fetch_assoc($res)["is_khusus"];

      if($is_khusus) 
      {
        $res = mysqli_query($conn, "SELECT jabatan.namajabatan, pendidikan.id_pendidikan, pendidikan.tingkatan FROM pendidikan_jabatan INNER JOIN jabatan ON pendidikan_jabatan.id_jabatan = jabatan.id_jabatan INNER JOIN pendidikan ON pendidikan_jabatan.id_pendidikan = pendidikan.id_pendidikan WHERE pendidikan.namapendidikan LIKE '%H' AND pendidikan_jabatan.id_jabatan = '". $jabatan ."'");
        $tingkatan = mysqli_fetch_assoc($res)["tingkatan"];

        $ress = mysqli_query($conn, "SELECT pegawai.id_pegawai, pegawai.nip, pegawai.namapegawai, pendidikan.namapendidikan, pegawai.jeniskelamin, pegawai.tanggallahir, unit.namaunit FROM pegawai INNER JOIN unit ON pegawai.unit = unit.id_unit INNER JOIN pendidikan on pegawai.pendidikan = pendidikan.id_pendidikan WHERE pendidikan.tingkatan >= '". $tingkatan ."' AND unit.namaunit = '". $_SESSION["namaunit"] ."' AND pendidikan.namapendidikan LIKE '%H' ORDER BY pendidikan.tingkatan DESC");
      }
      else
      {
        $res = mysqli_query($conn, "SELECT jabatan.namajabatan, pendidikan.id_pendidikan, pendidikan.tingkatan FROM pendidikan_jabatan INNER JOIN jabatan ON pendidikan_jabatan.id_jabatan = jabatan.id_jabatan INNER JOIN pendidikan ON pendidikan_jabatan.id_pendidikan = pendidikan.id_pendidikan WHERE NOT pendidikan.namapendidikan LIKE '%H' AND pendidikan_jabatan.id_jabatan = '". $jabatan ."'");
        $tingkatan = mysqli_fetch_assoc($res)["tingkatan"];

        $ress = mysqli_query($conn, "SELECT pegawai.id_pegawai, pegawai.nip, pegawai.namapegawai, pendidikan.namapendidikan, pegawai.jeniskelamin, pegawai.tanggallahir, unit.namaunit FROM pegawai INNER JOIN unit ON pegawai.unit = unit.id_unit INNER JOIN pendidikan on pegawai.pendidikan = pendidikan.id_pendidikan WHERE pendidikan.tingkatan >= '". $tingkatan ."' AND unit.namaunit = '". $_SESSION["namaunit"] ."' ORDER BY pendidikan.tingkatan DESC");
      }
      
  }
?>
<!-- tambah user -->
<div class="modal fade " role="dialog" id="myModal">
  <div class=" modal-dialog">
    <div class="box box-solid box-primary modal-content">

      <div class="box-header">
        <h3 class="box-title"><i class="ion-person-add"></i> &nbsp; kriteria</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
        </div>
      </div>

      <div class="box-body">
        <form action="" method="post" id="formiki">
          <input type="hidden" id="id_pg" name="id_pg" value="">
            <?php
              $query = "SELECT * FROM kriteria WHERE NOT namakriteria = 'Pendidikan'";
              $data = mysqli_query($conn, $query);
              while ($row = mysqli_fetch_assoc($data)) {
            ?>
          
            <div class="form-group">
              <label ><?= $row["namakriteria"] ?></label>
              <input disabled type="text" min="0" class="form-control" placeholder='<?= $row["namakriteria"] ?>' id='<?= $row["namakriteria"] ?>' name='data[<?= $row["namakriteria"] ?>]' value="">
              <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
            </div>
          <?php } ?>
          <div class="form-group">
            <label >Pendidikan</label>
            <select disabled class="form-control" id='Pendidikan' name='data[Pendidikan]'>
              <option value="">Pilih Pendidikan</option>
              <option value="100">S1</option>
              <option value="100">S1 H</option>
              <option value="80">D3</option>
              <option value="80">D3 H</option>
              <option value="60">SMA / SMK</option>
              <option value="60">SMA H / SMK H</option>
            </select>
            <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
          </div>
          

        </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Mutasi</h3>
      </div> <br>
      
      <div class="box-body table-responsive">
        <form action="" method="GET" enctype="multipart/form-data" name="frm_penilaian" class="form-inline">
          <input type="hidden" value="tambahmutasi" name="page" />
          <div class="form-group" style="width:39%">
            <label class="sr-only" for="exampleInputEmail3">Email address</label>
            
            <select class="form-control" style="width:100%" name="unit" id="unit" required>
              <option value="">----- Pilih Unit -----</option>
              <?php
                $r = mysqli_query($conn, "SELECT id_unit, namaunit FROM unit");
                while($row = mysqli_fetch_assoc($r)) {
              ?>
                <option value='<?= $row["id_unit"]?>'><?= $row["namaunit"]?></option>
              <?php } ?>
              
            </select>
          </div>
          <div class="form-group" style="width:39%">
            <label class="sr-only" for="exampleInputEmail3">Email address</label>
            <select class="form-control" style="width:100%" name="jabatan" id="jabatan" required>
              <option value="">----- Pilih Jabatan -----</option>
              <?php
                $r2 = mysqli_query($conn, "SELECT id_jabatan, namajabatan FROM jabatan");
                while($row = mysqli_fetch_assoc($r2)) {
              ?>
                <option value='<?= $row["id_jabatan"]?>'><?= $row["namajabatan"]?></option>
              <?php } ?>
            </select>
          </div>
          
          <div class="form-group" style="width:20%">
            <button type="submit" class="form-control btn btn-info" name="aksi" value="cari">Cari Data</button>
          </div>
          <!-- <div class="form-group" style="width:24%">
            <button type="submit" class="form-control btn btn-info" name="save">Simpan Data</button>
          </div> -->
      
        </form>
      </div>
    </div>

    <?php if(isset($_GET["aksi"])) { ?>
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Hasil Seleksi</h3>
      </div>
      
      <div class="box-body table-responsive">
      
      <form action="" method="POST" enctype="multipart/form-data" name="frm_mutasi" class="form-horizontal">

            <?php
              $no = 1;
              $datanilai = array();
              while($row = mysqli_fetch_assoc($ress)) {
                $datanilai[] = $row;
            ?>
                  <input type="hidden" value="<?= $row['id_pegawai'] ?>" name="data[]" />
            <?php } ?>

        <!-- Tabel Penilaian -->
        <table id="tabel" class="table table-bordered table-hover">
          <?php 
            $res = mysqli_query($conn, "SELECT namakriteria FROM kriteria ORDER BY id_kriteria ASC");
            $jumlah = mysqli_num_rows($res);
          ?>
          <thead>
            <tr rowspan="2">
            <th rowspan="2" width="2%" valign="middle">Urutan</th>
            <th rowspan="2" width="2%" valign="middle">NIP</th>
            <th rowspan="2"  valign="middle">Nama</th>
            <th rowspan="2"  valign="middle">Jabatan</th>
            <th rowspan="2"  valign="middle">Unit</th>
            <th rowspan="2" width="10%" valign="middle">Pendidikan</th>
            <th rowspan="2" width="5%" valign="middle">Tahun</th>
            <th rowspan="2" width="12%" class="text-center">Total Nilai</th>
          </thead>
          <tbody>
            <?php
                if($is_khusus) 
                {
                  $result = mysqli_query($conn, "SELECT jabatan.namajabatan,u.namaunit,p.nip,pen.namapendidikan,p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit, pg.tahun from pegawai as p  LEFT JOIN  penilaianpegawai as pg ON p.id_pegawai = pg.id_pegawai LEFT JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan LEFT JOIN unit as u ON p.unit = u.id_unit LEFT JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg INNER JOIN jabatan ON p.jabatan = jabatan.id_jabatan WHERE pen.tingkatan >='". $tingkatan."' AND u.namaunit = '". $_SESSION["namaunit"] ."' AND pen.namapendidikan LIKE '%H' GROUP BY pg.id_pg, p.id_pegawai, pg.tahun ORDER BY total DESC");       
                }
                else
                {
                  $result = mysqli_query($conn, "SELECT jabatan.namajabatan,u.namaunit,p.nip,pen.namapendidikan,p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit, pg.tahun from pegawai as p  LEFT JOIN  penilaianpegawai as pg ON p.id_pegawai = pg.id_pegawai LEFT JOIN pendidikan as pen ON p.pendidikan = pen.id_pendidikan LEFT JOIN unit as u ON p.unit = u.id_unit LEFT JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg INNER JOIN jabatan ON p.jabatan = jabatan.id_jabatan WHERE pen.tingkatan >='". $tingkatan."' AND u.namaunit = '". $_SESSION["namaunit"] ."'  GROUP BY pg.id_pg, p.id_pegawai, pg.tahun ORDER BY total DESC");
                }
                
                $i=1;
                while ($row = mysqli_fetch_assoc($result)) {
                    $r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd LEFT JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
                    echo "<tr>";
                    echo "<td align='center'>".$i."</td>
                    <td align='center'>".$row['nip']."</td>
                    <td id='nk".$row['id_pg']."'>".$row['namapegawai']."
                      
                    </td>
                    
                    <td align='center'>".$row['namajabatan']."</td>
                    <td align='center'>".$row['namaunit']."</td>
                    <td align='center'>".$row['namapendidikan']."</td>" ;
                    echo empty($row["tahun"]) ? "<td class='text-center' > - </td>" : "<td valign='middle' class='text-center'>".$row['tahun']."</td>";
                    
                    echo empty($row["total"]) ? "<td class='text-center'>-</td>" : "<td align='center'><input type='hidden' name='id_pg' id='id_pg' class='id_pg' value='".$row["id_pg"]."'>".$row["total"]." &nbsp; <span data-toggle='modal' data-target='#myModal' class='view-btn'><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Lihat Data'><i class='glyphicon glyphicon-eye-open'></i></a></span></td>";
                    echo "</tr>";
                    $i++;
                } 
            ?>
            <?php
              if(mysqli_num_rows($ress) < 1) {
            ?>
              <td colspan='<?= intval($jumlah + 4) ?>' class="text-center">Tidak ada yang memenuhi</td>
            <?php } ?>

          </tbody>
        </table><br><br>
        <div class="form-group">
          <div class="col-sm-12">
          <input type="hidden" name="id_unit" value='<?= isset($_GET["unit"]) ? $_GET["unit"] : "" ?>' />
            <input type="hidden" name="id_jabatan" value='<?= isset($_GET["jabatan"]) ? $_GET["jabatan"] : "" ?>' />
            <?php $disable = mysqli_num_rows($ress) < 1 ? "disabled" : ""; ?>
            <button type="submit" class="btn btn-info" name="save" <?= $disable ?> >Simpan Data</button>
          </div>
        </div>
      </form>
      </div>

    </div>
    <?php } ?>
  </div>
</div>
<!-- selesai tabel user -->


<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

 $("#tanggal").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        weekStart: 1,
        todayBtn:  1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $(".view-btn").on("click", function(e) {
      self = $(this);
      console.log(self);
      $parent = $(this).parents("td");
      $id_pg = $parent.find(".id_pg").val();
      $.post(base_url + "aksi/nilai_pegawai.php", {id_pg: $id_pg}, function(res){
        //console.log(res);
        $("#id_pg").val($id_pg);
        $.each(res, function(k, v) {
            //display the key and value pair
            // $()
            // $("input[name^=data["+ k +"]]").val(v);
            //console.log(v);
            $('input[name^="data['+k+']"]').val(v);
            $('select[name^="data['+k+']"]').val(v);
            //console.log($('input[name^="data['+k+']"]').html());
        });
      }, "json");
    })
</script>
