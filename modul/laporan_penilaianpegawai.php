
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<?php
  $kriteria=array();
  $q="select * from kriteria order by id_kriteria";
  $q=mysqli_query($conn, $q);
  while($h=mysqli_fetch_array($q)){

    // Push ke array kriteria
    // Format Array = [id_kriteria, id_kriteria, nama kriteria]
    // $kriteria[0][2] => muncul namanya
    $kriteria[]=array($h['id_kriteria'],$h['id_kriteria'],$h['namakriteria']);
  }
?>

<?php
  if(isset($_POST["save"]))
  {
    // Save data penilaian ke database. Fungsi berasal dari folder aksi/nilai_kriteria
    save_nilai_kriteria($conn, $kriteria);
    for($i=0;$i<count($kriteria);$i++)
    {
      $id_kriteria[]=$kriteria[$i][0];
    }
    $matrik_kriteria = ahp_get_matrik_kriteria($conn, $id_kriteria);
    $jumlah_kolom = ahp_get_jumlah_kolom($matrik_kriteria);
    $matrik_normalisasi = ahp_get_normalisasi($matrik_kriteria, $jumlah_kolom);
    $prioritas_kriteria = ahp_get_eigen($matrik_normalisasi);
    //$q1 = mysqli_query($conn, "TRUNCATE prioritas_kriteria");
    foreach($prioritas_kriteria as $key => $val)
    {
      $query = "UPDATE prioritas_kriteria SET nilai = '". $val ."' WHERE id_kriteria ='". $kriteria[$key][1] . "'";
      //$query = "INSERT INTO prioritas_kriteria (id_kriteria, nilai) VALUES ('" . $kriteria[$key][1]. "', '". $val ."')";
      $q = mysqli_query($conn, $query);
    }
    $info = "update";
    echo "<script> document.location.href = base_url+'index.php?page=penilaiankriteria&info=".$info."'</script>";
  }
?>

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Laporan Penilaian Pegawai</h3>
        <button class="btn btn-info pull-right" type="button" onClick="printdiv('printarea');">Cetak Laporan</button>
      </div> <br>

      <style type="text/css">
          .printMe {display: none;}
          @media print {
              div {display: none;}
              .printMe {display: block;}
          }
      </style>

      <script language="javascript">
      function printdiv(printpage)
      {
      var headstr = "<html><head><title></title></head><body>";
      var footstr = "</body>";
      var newstr = document.all.item(printpage).innerHTML;
      var oldstr = document.body.innerHTML;
      document.body.innerHTML = headstr+newstr+footstr;
      window.print();
      // setTimeout(function(){ window.print() }, 100);
      document.body.innerHTML = oldstr;
      return false;
      }
      </script>
      
      <div class="box-body table-responsive">
      <form action="" method="GET" enctype="multipart/form-data" name="frm_penilaian">
        <?php
            $res = mysqli_query($conn, "SELECT namakriteria FROM kriteria ORDER BY id_kriteria ASC");
        ?>
        <div class="form-group">
            <input type="hidden" name="page" value="laporan_penilaianpegawai">
            <label >Tahun Penilaian</label>
            <input type="text" class="form-control" placeholder='tahun' id='tahun' name='tahun' value='<?= isset($_GET["tahun"]) ? $_GET["tahun"] : "" ?>'></input>
            
        </div>
           <div class="form-group">
                <label >Unit</label>
                 <?php
                  echo "<select class='form-control' id='kopunit' name='unit'>";
                    $unit = mysqli_query($conn, "SELECT * FROM unit");
                  echo "<option value='' selected disabled> - Pilih Unit - </option>";
                  while ($a = mysqli_fetch_assoc($unit)) {
                    echo "<option value='".$a['namaunit']."'>".$a['namaunit']."</option>";
                  }
              echo "</select>";

              $a = isset($_GET["tahun"]) ? $_GET["tahun"] : "" ;
              $b = isset($_GET["unit"]) ? $_GET["unit"] : "" ;
            ?>
        </div>
        <div style="clear:both;"></div>
        <button type="submit" class="btn btn-info pull-right" >Cari Data</button>
        <div style="clear:both;"></div><br />
<div id="printarea">   
          <div class="printMe">
            <script type="text/javascript">document.write('<center><h1><b>' + document.title  + '</b></h1></center>')</script>
            <?php 

            function random($length = 4) {
                $chars = '0123456789abc';

                $Code = '';
                for ($i = 0; $i < $length; ++$i) {
                    $Code .= substr($chars, (((int) mt_rand(0, strlen($chars))) - 1), 1);
                }
                return strtoupper($Code);
            }


            ?>
            <center>
            <h2><b>LAPORAN PENILAIAN PEGAWAI</b></h2> <br>
            <small>Nomor Surat : SK-LAP/LAP-<?php print_r(random(4))?>/v/<?php echo date("Y"); ?> </small> <br>
            <small>Dikeluarkan oleh : <?php echo $_SESSION['status'] ?> </small>
            </center>
            
            <br>

            <div class="pull-left">
            <h3> Tahun Penilaian : <?php echo $a ?> </h3> <p> 
            <h3> Unit : <?php echo $b; ?></h3> 
            </div>  <br><br>


          </div>

        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr rowspan="2">
              <th rowspan="2" width="2%" valign="middle">Urutan</th>
              <th rowspan="2" width="20%" valign="middle">Nama Pegawai</th>
              <th rowspan="2" width="20%" valign="middle">Unit</th>
              <th colspan='<?= mysqli_num_rows($res)?>' class="text-center" >Nilai Kriteria</th>
              
              <th rowspan="2" width="10%" class="text-center">Total</th>
             </tr>
             <tr>
                <?php
                    
                    while($row = mysqli_fetch_assoc($res)) {
                ?>
                        <th><?= $row["namakriteria"]?></th>
                    <?php } ?>
             </tr>
          </thead>
          <tbody>
            <?php
              if(isset($_GET["tahun"]) AND isset($_GET["unit"])) {
                
                $result = mysqli_query($conn, "SELECT p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit from penilaianpegawai as pg INNER JOIN pegawai as p ON pg.id_pegawai = p.id_pegawai INNER JOIN unit as u ON p.unit = u.id_unit INNER JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg WHERE pg.tahun ='". $_GET["tahun"]."' AND u.namaunit ='". $_GET["unit"]."' GROUP BY pg.id_pg ORDER BY total DESC");
                $i=1;
                while ($row = mysqli_fetch_assoc($result)) {
                    $r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd INNER JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
                    echo "<tr>";
                    echo "<td align='center'>".$i."</td>
                    <td id='nk".$row['id_pg']."'>".$row['namapegawai']."</td>";
                    echo "<td align='center'>".$row['namaunit']."</td>";
                    while($child = mysqli_fetch_assoc($r)) {
                        echo "<td>".$child["subtotal_nilai"]. "</td>";
                    }
                    echo "<td align='center'>".$row["total"]."</td>";
                    echo "</tr>";
                    $i++;
                }
              } 
              // else if(isset($_GET["tahun"])) {
                
              //   $result = mysqli_query($conn, "SELECT p.id_pegawai, p.namapegawai, pg.id_pg, SUM(pgd.subtotal_nilai) as total, p.unit, u.namaunit from penilaianpegawai as pg INNER JOIN pegawai as p ON pg.id_pegawai = p.id_pegawai INNER JOIN unit as u ON p.unit = u.id_unit INNER JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg WHERE pg.tahun ='". $_GET["tahun"]."'GROUP BY pg.id_pg ORDER BY total DESC");
              //   $i=1;
              //   while ($row = mysqli_fetch_assoc($result)) {
              //       $r = mysqli_query($conn, "SELECT * FROM p_pegawai_detail as pgd INNER JOIN prioritas_kriteria as e ON pgd.id_prioritas = e.id_prioritas WHERE id_pg = '". $row["id_pg"] . "' ORDER BY e.id_kriteria ASC");
              //       echo "<tr>";
              //       echo "<td align='center'>".$i."</td>
              //       <td id='nk".$row['id_pg']."'>".$row['namapegawai']."</td>";
              //       echo "<td align='center'>".$row['namaunit']."</td>";
              //       while($child = mysqli_fetch_assoc($r)) {
              //           echo "<td>".$child["subtotal_nilai"]. "</td>";
              //       }
              //       echo "<td align='center'>".$row["total"]."</td>";
              //       echo "</tr>";
              //       $i++;
              //   }
              // } 
            ?>

          </tbody>
        </table>
</div>       
        
        </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<!-- session storage for selected dropdown -->
<script type="text/javascript">
  window.onload = function() {
    var selItem = sessionStorage.getItem("kopunit");  
    $('#kopunit').val(selItem);
    }
    $('#kopunit').change(function() { 
        var selVal = $(this).val();
        sessionStorage.setItem("kopunit", selVal);
    });
</script>

<?php
  if(isset($_POST['simpan'])){
    $id_kriteria = $_POST['id_kriteria'];
    $namakriteria = $_POST['namakriteria'];

    if ($id_kriteria == "") {
      $query ="INSERT INTO kriteria (namakriteria) VALUES ('$namakriteria')";
      $info ="tambah";
    }
    elseif($id_kriteria != "") {
      $query = "update kriteria set namakriteria='$namakriteria' where id_kriteria='$id_kriteria'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from kriteria where id_kriteria='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=kriteria&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
    namaunit: {required: true}, 
      
    },
    messages: {
    namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
  nk = $("#nk"+id).text();
 
  $("#namakriteria").val(nk);
    
  $("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
  
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

  $("#tahun").datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });
</script>
