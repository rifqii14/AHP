<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>


<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box">

      <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fw fa-user"></i>List Pegawai</h3>
        <div class="box-tools pull-right">
          <button class="btn" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>

      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>NIP</th>
            <th>Nama Pegawai</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th width="">Alamat</th>
            <th width="13%">Jenis Kelamin</th>
            <th width="5%">Golongan Darah</th>
            <th>Pendidikan</th>
            <th width="14%">Jabatan</th>
            <th width="10%">Aktif</th>
           </tr>
          </thead>
          <tbody>
             <?php
              $result = mysqli_query($conn, "SELECT * from pegawai 
                inner join pendidikan on pendidikan.id_pendidikan = pegawai.pendidikan 
                inner join jabatan on jabatan.id_jabatan = pegawai.jabatan
                inner join unit on unit.id_unit = pegawai.unit where namaunit ='".$_SESSION['namaunit']."' order by namapegawai asc");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                $aktif = $row["aktif"] ? "disabled" : "";
                $tidakaktif = $row["aktif"] ? "" : "disabled"; 
             ?>
                 <tr>
                  <td align='center'><?= $i++ ?></td>
                  <td><?= $row['nip']?></td>
                  <td><?= $row['namapegawai']?></td>
                  <td><?= $row['tempatlahir']?></td>
                  <td><?= tanggal_format_indonesia($row['tanggallahir'])?></td>
                  <td><?= $row['alamat']?></td>
                  <td><?= $row['jeniskelamin']?></td>
                  <td><?= $row['goldarah']?></td>
                  <td><?= $row['namapendidikan']?></td>
                  <td><?= $row['namajabatan']?></td>
                  <td>
                    <form action="" method="POST" id="frm_aktif" name="frm_aktif">
                      <input type="hidden" name="id_pegawai" value="<?= $row['id_pegawai']?>">
                      <div class="btn-group btn-group-sm" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success" <?= $aktif ?> name="btn_aktif"><i class="glyphicon glyphicon-ok"></i></button>
                        <button type="submit" class="btn btn-danger" <?= $tidakaktif ?>  name="btn_tidakaktif"><i class="glyphicon glyphicon-remove"></i></button>
                      </div>
                    </form>
                  </td>
                </tr>
              <?php } ?>

          </tbody>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['kirim'])){
    $id_pegawai = $_POST['id_pegawai'];
    $namapegawai = $_POST['namapegawai'];
    $tempatlahir = $_POST['tempatlahir'];
    $tanggallahir = $_POST['tanggallahir'];
    $alamat = $_POST['alamat'];
	$jeniskelamin = $_POST['jeniskelamin'];
    $goldarah = $_POST['goldarah'];
	$pendidikan = $_POST['pendidikan'];
	$jabatan = $_POST['jabatan'];
    $unit = $_POST['unit'];
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mpegawai&info=".$info."'</script>";
  }

  /* Aksi Set Aktif Pegawai */

  if(isset($_POST["btn_aktif"])) {
      $id_pegawai = $_POST["id_pegawai"];
      $query = "UPDATE pegawai SET aktif = TRUE WHERE id_pegawai = '$id_pegawai'";
      $res = mysqli_query($conn, $query);
      $info = "update";
      if($res)
        echo "<script> document.location.href = base_url+'index.php?page=Mpegawaiketuaunit&info=".$info."'</script>";
  }

  if(isset($_POST["btn_tidakaktif"])) {
    $id_pegawai = $_POST["id_pegawai"];
    $query = "UPDATE pegawai SET aktif = FALSE WHERE id_pegawai = '$id_pegawai'";
    $res = mysqli_query($conn, $query);
    $info = "update";
    if($res)
      echo "<script> document.location.href = base_url+'index.php?page=Mpegawaiketuaunit&info=".$info."'</script>";
}

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  
</script>
