<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>
<script src="sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="sweetalert.css">
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form jabatan</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
        <input type="hidden" name="aksi-h" id="aksi-h" value="tambah" />
          <form action="" method="post" id="formiki" action="index.php?page=jabatan">
          
            <div class="form-group">
                <label>Nama Jabatan</label>
                <input type="text" class="form-control" placeholder="Nama Jabatan" id="namajabatan" name="namajabatan"></input>
                <span style="color:red" id="err-jab" >Nama Sudah digunakan. Mohon gunakan nama lain</span>
                <input type="hidden" id="id" name="id_jabatan" value="" ></input>
            </div>
            
                        
            <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan" id="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Jabatan</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' style="margin: 10px" id="tambah-p"><i class="fa fa-plus"></i> Tambah Jabatan</button>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Nama Jabatan</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from jabatan");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
				  <td id='nj".$row['id_jabatan']."'>".$row['namajabatan']."</td>
                  <td align='center'>
                   <span class='edit-p' data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_jabatan'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_jabatan'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['namajabatan'])){
    $id_jabatan = $_POST['id_jabatan'];
    $namajabatan = $_POST['namajabatan'];	  

    if ($id_jabatan == "") {
      $query ="INSERT INTO jabatan (namajabatan) VALUES ('$namajabatan')";
      $info ="tambah";
    }
    elseif($id_jabatan != "") {
      $query = "update jabatan set namajabatan='$namajabatan' where id_jabatan='$id_jabatan'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=jabatan&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from jabatan where id_jabatan='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=jabatan&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
<script type="text/javascript">
  $("#err-jab").hide();
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  
  function edit(id){
	nj = $("#nj"+id).text();
 
	$("#namajabatan").val(nj);
    
	$("#id").val(id);
  }
	
	
function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=jabatan&hapus='+id;
  }

  $('#simpan').on('click', function(e) {
  
    var validator = $("#formiki").validate({
      rules: {
      namajabatan: {required: true},	
        
      },
      messages: {
      namajabatan: {required: "Nama jabatan tidak boleh kosong"},
        
      }
    });

  if($("#formiki").valid()) {
    var self = $(this).parents("#formiki");
    $namajabatan = self.find("#namajabatan").val();
    //console.log ($namajabatan);
    $aksiform = $("#aksi-h").val();
    if($aksiform === "add") {
    $.post(base_url + "aksi/cek_jabatan.php", {namajabatan : $namajabatan}, function(res){
      if(res !== "ada"){
        $("#err-jab").hide();
        //console.log(res);
        document.getElementById("formiki").submit();
      } else {
        //console.log(res);
        $("#err-jab").show();
        
      }
    }, "json");
    
    }else {
      document.getElementById("formiki").submit();
    }
    e.preventDefault();
  }
});

  $("#tambah-p").on("click", function() {
    $("#aksi-h").val("add");
  });
  $("#tabel").on("click", ".edit-p", function() {
    $("#aksi-h").val("edit");
    $("#err-jab").hide();
  });
</script>
