<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>

<?php
  if(isset($_POST["simpan"]))
  {
    if($_POST["aksi"] == "tambah") {
      $res = save_nilai_pegawai($conn);
      if($res)
      {
        $info ="tambah";
        echo "<script> document.location.href = base_url+'index.php?page=penilaianpegawai&info=".$info."'</script>";
      }
    } else {
      $res = update_nilai_pegawai($conn);
      if($res)
      {
        $info ="update";
        echo "<script> document.location.href = base_url+'index.php?page=penilaianpegawai&info=".$info."'</script>";
      }
    }
  }
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form kriteria</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <form action="" method="post" id="formiki">
            <input type="hidden" id="aksi" name="aksi" value="">
            <input type="hidden" id="id_pg" name="id_pg" value="">
            <div class="form-group">
              <label >Tahun</label>
              <input type="text" class="form-control" placeholder='Tahun' id='tahun' name='tahun' value="" required>
              <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
            </div>
            
            <div class="form-group">
              <label >Nama Pegawai</label>
              <select name="id_pegawai" id="id_pegawai" class="form-control" required>
                <option value="">------Pilih Pegawai-------</option>
                <?php 
                  $r = mysqli_query($conn, "SELECT id_pegawai, namapegawai FROM pegawai INNER JOIN unit ON pegawai.unit = unit.id_unit WHERE unit.namaunit = '".$_SESSION['namaunit']."'"); 
                  while($row = mysqli_fetch_assoc($r)) {
                ?>
                  <option value="<?= $row['id_pegawai']?>"><?= $row['namapegawai']?></option>
                <?php } ?>
              </select>
            </div>
            <hr />
              <?php
                $query = "SELECT * FROM kriteria WHERE NOT namakriteria = 'Pendidikan'";
                $data = mysqli_query($conn, $query);
                while ($row = mysqli_fetch_assoc($data)) {
              ?>
            
              <div class="form-group">
                <label ><?= $row["namakriteria"] ?></label>
                <input type="number" min="0" class="form-control" placeholder='<?= $row["namakriteria"] ?>' id='<?= $row["namakriteria"] ?>' name='data[<?= $row["namakriteria"] ?>]' value="">
                <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
              </div>
            <?php } ?>
            <div class="form-group">
              <label >Pendidikan</label>
              <select class="form-control" id='Pendidikan' name='data[Pendidikan]'>
                <option value="">Pilih Pendidikan</option>
                <option value="100">S1</option>
                <option value="100">S1 H</option>
                <option value="80">D3</option>
                <option value="80">D3 H</option>
                <option value="60">SMA / SMK</option>
                <option value="60">SMA H / SMK H</option>
              </select>
              <!--<input type="text" id="id" name='<?= $row["id_kriteria"] ?>' value="" hidden></input> -->
            </div>
            
           <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Pegawai</h3>
      </div> <br>
      
      <div class="box-body table-responsive">
        <form action="" method="POST" name="frmpg" id="frmpg">
          <a class="btn btn-primary " href="?page=tambah_pg" name="save_pg" id="add" style=""><i class="fa fa-plus"></i> Tambah Data Penilaian</a>
          <br />
          <br />
          <table id="tabel" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nama Pegawai</th>
                <th>Tahun</th>
                <th width="">Total Nilai</th>
                <th width="9%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $result = mysqli_query($conn, "SELECT pg.id_pg, pg.id_pegawai, p.namapegawai, CAST(pg.tahun AS UNSIGNED) as tahun , SUM(pgd.subtotal_nilai) as total, u.namaunit FROM `penilaianpegawai` as pg INNER JOIN pegawai as p ON pg.id_pegawai = p.id_pegawai INNER JOIN unit as u ON p.unit = u.id_unit INNER JOIN p_pegawai_detail as pgd ON pg.id_pg = pgd.id_pg WHERE u.namaunit = '". $_SESSION["namaunit"] ."' GROUP BY pg.id_pg ORDER BY tahun DESC");
                $i=1;
                while ($row = mysqli_fetch_assoc($result)) {
                  echo "<tr>
                    
                    <td id='nk".$row['id_pegawai']."'>".$row['namapegawai']."</td>
                    <td align='center' id='col-tahun'>".$row["tahun"]."</td>
                     <td align='center'>".$row["total"]."</td>
                    <td align='center'>
                    <input type='hidden' value='".$row['id_pg']."' class='id_pg'>
                    <input type='hidden' value='".$row['id_pegawai']."' class='id_pegawai'>
                    <span data-toggle='modal' data-target='#myModal' class='edit-btn' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span>
                      <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_pg'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                    </td>
                  </tr>";
                  $i++;
                }
              ?>

            </tbody>
          </table>
        </form>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  
  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from p_pegawai_detail where id_pg='$id'";
    $result = mysqli_query($conn, $query);
    $query = "delete from penilaianpegawai where id_pg='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=penilaianpegawai&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable({
    aaSorting: [[1, 'desc']]
  });
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=penilaianpegawai&hapus='+id;
  }
</script>

<script type="text/javascript">
  $("#tahun").datetimepicker({
      format: "yyyy",
      startView: 'decade',
      minView: 'decade',
      viewSelect: 'decade',
      autoclose: true,
  });

  $("#frmpg").on("click", ".edit-btn", function(e) {
    self = $(this);
    $parent = $(this).parents("td");
    $id_pg = $parent.find(".id_pg").val();
    $id_pgw = $parent.find(".id_pegawai").val();
    $.post(base_url + "aksi/nilai_pegawai.php", {id_pg: $id_pg}, function(res){
      //console.log(res);
      $("#id_pg").val($id_pg);
      $("#aksi").val("edit");
      $("#tahun").val(self.parents("tr").find("#col-tahun").html());
      $("#id_pegawai").val($id_pgw);
      $.each(res, function(k, v) {
          //display the key and value pair
          // $()
          // $("input[name^=data["+ k +"]]").val(v);
          //console.log(v);
          $('input[name^="data['+k+']"]').val(v);
          $('select[name^="data['+k+']"]').val(v);
          //console.log($('input[name^="data['+k+']"]').html());
      });
    }, "json");
  })

  $("#add").on("click", function(){
    $("#formiki").trigger("reset");
    $("#aksi").val("tambah");
    $("#id_pg").val("");
  })
  
</script>
