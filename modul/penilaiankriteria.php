
<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<?php
  
?>
<?php
  $r = mysqli_query($conn, "SELECT id_kriteria, namakriteria FROM kriteria ORDER BY id_kriteria ASC");
  $kriteria = array();
  while($row = mysqli_fetch_assoc($r)) {
    $kriteria[] = array($row["id_kriteria"], $row["namakriteria"]);
  }
?>
<?php
  if(isset($_POST["save"]))
  {
    // Save data penilaian ke database. Fungsi berasal dari folder aksi/nilai_kriteria
    $hasil = update_nilai_kriteria($conn);
    for($i=0;$i<count($kriteria);$i++)
    {
      $id_kriteria[]=$kriteria[$i][0];
    }

    $matrik_kriteria = ahp_get_matrik_kriteria($conn, $id_kriteria);
    $jumlah_kolom = ahp_get_jumlah_kolom($matrik_kriteria);
    $matrik_normalisasi = ahp_get_normalisasi($matrik_kriteria, $jumlah_kolom);
    $prioritas_kriteria = ahp_get_eigen($matrik_normalisasi);

    foreach($prioritas_kriteria as $key => $val)
    {
      $query = "UPDATE prioritas_kriteria SET nilai = '". round($val, 2) ."' WHERE id_kriteria ='". $kriteria[$key][0] . "'";
      $q = mysqli_query($conn, $query);
    }

    $info = "update";
    
    if($hasil) { 
      echo "<script> document.location.href = base_url+'index.php?page=penilaiankriteria&info=".$info."'</script>";
    }
  }
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form kriteria</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <form action="" method="post" id="formiki">
          
            <div class="form-group">
              <label >Nama Kriteria</label>
              <input type="text" class="form-control" placeholder="Nama Kriteria" id="namakriteria" name="namakriteria" value=""></input>
              <input type="text" id="id" name="id_kriteria" value="" hidden></input>
            </div>
            
           <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Penilaian Kriteria</h3>
      </div> <br>
      
      <div class="box-body table-responsive">
      <form action="" method="POST" enctype="multipart/form-data" name="frm_penilaian" class="form-inline">
        <div class="form-group" style="width:24%">
          <label class="sr-only" for="exampleInputEmail3">Email address</label>
          
          <select class="form-control" style="width:100%" name="kriteria1" id="kriteria1">
            <option>----- Pilih Kriteria 1 -----</option>
            <?php
              mysqli_data_seek($r,0);
              while($row = mysqli_fetch_assoc($r)) {
            ?>
              <option value='<?= $row["id_kriteria"]?>'><?= $row["namakriteria"]?></option>
            <?php } ?>
            
          </select>
        </div>
        <div class="form-group" style="width:24%">
          <label class="sr-only" for="exampleInputEmail3">Email address</label>
          <select class="form-control" style="width:100%" name="perbandingan">
            <option>----- Pilih Perbandingan -----</option>
            <option value="1">1. Sama penting dengan</option>
            <option value="2">2. Mendekati sedikit lebih penting dari</option>
            <option value="3">3. Sedikit lebih penting dari</option>
            <option value="4">4. Mendekati lebih penting dari</option>
            <option value="5">5. Lebih penting dari</option>
            <option value="6">6. Mendekati sangat penting dari</option>
            <option value="7">7. Sangat penting dari</option>
            <option value="8">8. Mendekati mutlak dari</option>
            <option value="9">9. Mutlak sangat penting dari</option>
          </select>
        </div>
        <div class="form-group" style="width:24%">
          <label class="sr-only" for="exampleInputEmail3">Email address</label>
          <select class="form-control" style="width:100%" name="kriteria2" id="kriteria2">
            <option value="">----- Pilih Kriteria 2 -----</option>
            <?php
              mysqli_data_seek($r,0);
              while($row = mysqli_fetch_assoc($r)) {
            ?>
              <option value='<?= $row["id_kriteria"]?>'><?= $row["namakriteria"]?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group" style="width:24%">
          <button type="submit" class="form-control btn btn-info" name="save">Simpan Data</button>
        </div>
        
    
      </form>

      <?php
        for($i=0;$i<count($kriteria);$i++)
        {
          $id_kriteria1[]=$kriteria[$i][0];
        }

        for($i = 0; $i < count($kriteria); $i++)
        {
          $reigen = mysqli_query($conn, "SELECT id_prioritas, nilai FROM prioritas_kriteria WHERE id_kriteria = '".$kriteria[$i][0]."'");
          if(mysqli_num_rows($reigen) < 1)
          {
            $q=mysqli_query($conn, "INSERT INTO prioritas_kriteria (id_kriteria, nilai) VALUES ('".$kriteria[$i][0]."', '0.00')");
            $matrik_kriteria = ahp_get_matrik_kriteria($conn, $id_kriteria1);
            $jumlah_kolom = ahp_get_jumlah_kolom($matrik_kriteria);
            $matrik_normalisasi = ahp_get_normalisasi($matrik_kriteria, $jumlah_kolom);
            $prioritas_kriteria = ahp_get_eigen($matrik_normalisasi);

            foreach($prioritas_kriteria as $key => $val)
            {
              $query = "UPDATE prioritas_kriteria SET nilai = '". round($val, 2) ."' WHERE id_kriteria ='". $kriteria[$key][0] . "'";
              $qu = mysqli_query($conn, $query);
            }
          }

          for($j = 0; $j < count($kriteria); $j++)
          {
            $perbandingan = mysqli_query($conn, "SELECT id_pk FROM penilaiankriteria WHERE id_kriteria_1 ='". $kriteria[$i][0] ."' AND id_kriteria_2 = '". $kriteria[$j][0] ."'");
            if(mysqli_num_rows($perbandingan) < 1)
            {
              $ins = mysqli_query($conn, "INSERT INTO penilaiankriteria (id_kriteria_1, id_kriteria_2, nilai) VALUES ('".$kriteria[$i][0]."', '". $kriteria[$j][0]."', 1)");
            }  
          }
        }
      ?>
      <table class="table table-hover table-bordered">
        <thead>
          <tr>
            <th>&nbsp;</th>
            <?php
              mysqli_data_seek($r,0);
              while($row = mysqli_fetch_assoc($r)) {
            ?>
              <th class="text-center"><?= $row["namakriteria"] ?></th>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php
            for($i = 0; $i < count($kriteria); $i++)
            {
          ?>
            <tr>
              <td class="text-right"><strong><?= $kriteria[$i][1]?> </strong></td>
            
            <?php
              $nbanding = mysqli_query($conn, "SELECT nilai FROM penilaiankriteria WHERE id_kriteria_1 ='". $kriteria[$i][0] ."' ORDER BY id_kriteria_2");
              while($row = mysqli_fetch_assoc($nbanding)) {
            ?>
              <td class="text-center"><?= $row["nilai"] ?></td>
            <?php } ?>

            </tr>
          <?php } ?>
        </tbody>
      </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->


<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
//   $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
	  namaunit: {required: true},	
      
    },
    messages: {
	  namaunit: {required: "Nama Unit tidak boleh kosong"},
      
    }
  });
  function edit(id){
	nk = $("#nk"+id).text();
 
	$("#namakriteria").val(nk);
    
	$("#id").val(id);
  }

 function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=kriteria&hapus='+id;
  }

  $("#kriteria1").on("change", function(e) {
    $("#kriteria2").val("");
    $("#kriteria2 option").removeAttr('disabled');
    $kriteriaVal = $(this).val();
    $("#kriteria2 option[value=" + $kriteriaVal + "]").attr('disabled','disabled');
  });
</script>
