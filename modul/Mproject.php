<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
  if(isset($_GET['info'])){
    switch ($_GET['info']) {
      case 'tambah':
        echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
        break;
      case 'update':
        echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
        break;
      case 'hapus':
        echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
        break;
    }
  }
?>


<!-- tabel project -->
<div class="row">
  <div class="col-lg-12">
    <div class="box">

      <div class="box-header">
        <h3 class="box-title"><i class="fa fa-fw fa-user"></i>List Proyek</h3>
        <div class="box-tools pull-right">
          <button class="btn" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>

      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Proyek</th>
              <th>Nama Proyek</th>
              <th>Nama Client</th>
              <th>Biaya (Rp.)</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Selesai</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $query =" select p.*, c.id_client, c.nama_client
                        from proyek p
                        join client c
                        on p.id_client=c.id_client
                        order by p.created_at desc";
              $result = mysqli_query($conn, $query);
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>
                  <td align='center'>".$i."</td>
                  <td>".$row['kode_proyek']."</td>
                  <td>".$row['nama_proyek']."</td>
                  <td>".$row['nama_client']."</td>
                  <td>".format_rupiah($row['biaya'])."</td>
                  <td>".tanggal_format_indonesia($row['tanggal_mulai'])."</td>
                  <td>".tanggal_format_indonesia($row['tanggal_selesai'])."</td>
                  <td>".$row['status']."</td>
                  <td align='center'>";
                    echo "<a href='?page=Mdetail.project&id=".$row['id']."'> <span class='glyphicon glyphicon-tasks' data-toggle='tooltip' data-original-title='Detail'></span> </a>&nbsp";                    
                      echo "<a href='?page=Mprogres.project&id=".$row['id']."'> <span class='glyphicon glyphicon-stats' data-toggle='tooltip' data-original-title='Progress'></span> </a>&nbsp";
                   "</td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- selesai tabel project -->

<?php
  if(isset($_POST['kirim'])){
    $kode_proyek = $_POST['kode_proyek'];
    $nama_proyek = $_POST['nama_proyek'];
    $biaya = $_POST['biaya'];
    $tanggal_mulai = $_POST['tanggal_mulai'];
    $tanggal_selesai = $_POST['tanggal_selesai'];
    $id_client = $_POST['id_client'];

    $query= "INSERT INTO proyek (kode_proyek, id_client, nama_proyek, biaya, tanggal_mulai, tanggal_selesai, status)
  		    VALUES ('$kode_proyek','$id_client', '$nama_proyek', '$biaya', '$tanggal_mulai', '$tanggal_selesai', 'Proses' )";

    $result = mysqli_query($conn, $query);
    // header('location:beranda.php?page=dataproyek.php');
        echo "<script> document.location.href = base_url+'index.php?page=Mproject&info=tambah'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from proyek where id='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=Mproject&info=hapus'</script>";
  }
 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
$('#tipe_proyek').change(function() {
    var tipe = $(this).val();
    $.ajax({
        url: base_url + "lib.php",
        type: 'GET',
        data: {
            tipe: tipe,
            funct:"getNewCodeProject"
        },
        success: function(data)
        {
            $('#kode_proyek').val(data);
        }
    });
});

$(".alert" ).fadeOut(8000);
var validator = $("#formiki").validate({
  rules: {
    tipe_proyek: {required: true},
    nama_proyek: {required: true},
    id_client: {required: true},
    biaya: {required: true, number:true},
    tanggal_mulai: {required: true},
    tanggal_selesai: {required: true},
  },
  messages: {
    tipe_proyek: {required: "Tipe proyek harus dipilih"},
    nama_proyek: {required: "Nama proyek tidak boleh kosong"},
    id_client: {required: "Client tidak boleh kosong"},
    biaya: {required: "Biaya tidak boleh kosong", number:"Harus berupa angka"},
    tanggal_mulai: {required: "Tanggal mulai tidak boleh kosong"},
    tanggal_selesai: {required: "Tanggal mulai tidak boleh kosong"},
  }
});

function hapus(id){
 document.location.href = base_url+'index.php?page=project&hapus='+id;
}

$('#tabel').dataTable();

$('.datepicker').datetimepicker({
  language:  'id',
  weekStart: 1,
  todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
});
</script>
