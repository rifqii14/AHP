<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>
<script src="sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="sweetalert.css">
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form Pegawai</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <input type="hidden" name="aksi-h" id="aksi-h" value="tambah" />
          <form method="post" id="formiki" action="index.php?page=pegawai">
          
            <div class="form-group">
              <label >NIP</label>
              <input type="text" class="form-control" placeholder="NIP" id="nip" name="nip"></input>
              <span style="color:red" id="err-nip" >NIP sudah digunakan. Mohon masukan NIP lain</span>
            </div>
          
 			<div class="form-group">
              <label >Nama Pegawai</label>
              <input type="text" class="form-control" placeholder="Nama Pegawai" id="namapegawai" name="namapegawai" value=""></input>
              <input type="text" id="id" name="id_pegawai" value="" hidden></input>
            </div>

            <div class="form-group">
              <label >Tempat Lahir</label>
              <input type="text" class="form-control" placeholder="Tempat Lahir" id="tempatlahir" name="tempatlahir"></input>
            </div>

           <div class="input-group">
                <span class="input-group-addon">Tanggal Lahir</span>
                <input type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd" placeholder="Tanggal Lahir" name="tanggallahir"></input>
              </div>
           
            <div class="form-group">
              <label >Alamat</label>
              <textarea class="form-control" placeholder="Alamat" id= "alamat" name="alamat"></textarea>
            </div>
			
            <div class="form-group">
              <label >Jenis Kelamin</label>
              <select class="form-control" id="jeniskelamin" name="jeniskelamin">
                <option value=""> - Pilih Jenis Kelamin - </option>
                <option value='Pria'>Pria</option>
                <option value='Wanita'>Wanita</option>
              </select>
            </div>
            
		     <div class="form-group">
              <label >Golongan Darah</label>
              <select class="form-control" id="goldarah" name="goldarah">
                <option value=""> - Pilih Jenis Golongan Darah - </option>
                <option value='A'>A</option>
                <option value='B'>B</option>
                <option value='AB'>AB</option>
                <option value='O'>O</option>
               </select>
            </div>
            
            <div class="form-group">
              <label >Pendidikan</label>
              <?php
                echo "<select class='form-control' id='pendidikan' name='pendidikan'>";
                  $pendidikan = mysqli_query($conn, "select * from pendidikan");
                echo "<option value=''> - Pilih Jenis Pendidikan - </option>";
                while ($a = mysqli_fetch_assoc($pendidikan)) {
                  echo "<option value='".$a['id_pendidikan']."'>".$a['namapendidikan']."</option>";

                }

                echo "</select>";

              ?>
            </div>
            
            <div class="form-group">
              <label >Jabatan</label>
              <?php
                echo "<select class='form-control' id='jabatan' name='jabatan'>";
                  $jabatan = mysqli_query($conn, "select * from jabatan");
                echo "<option value=''> - Pilih Jenis Jabatan - </option>";
                while ($a = mysqli_fetch_assoc($jabatan)) {
                  echo "<option value='".$a['id_jabatan']."'>".$a['namajabatan']."</option>";

                }

                echo "</select>";

              ?>
            </div>
            
             <div class="form-group">
             <label> Unit </label>
              <?php
                echo "<select class='form-control' id='unit' name='unit'>";
                  $unit = mysqli_query($conn, "select * from unit");
                echo "<option value=''> - Pilih Jenis Unit - </option>";
                while ($a = mysqli_fetch_assoc($unit)) {
                  echo "<option value='".$a['id_unit']."'>".$a['namaunit']."</option>";

                }

                echo "</select>";

              ?>
            </div>
            
            <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan" id="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Pegawai</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' id="tambah-p" style="margin: 10px"><i class="fa fa-plus"></i> Tambah Pegawai</button>

      <div class="box-body table-responsive">

        
          <form name="cari" method="POST" action="">
               <div class="form-group">
                    <label >BERDASARKAN UNIT</label>
                     <?php
                      echo "<select class='form-control' id='cariunit' name='unit'>";
                        $unit = mysqli_query($conn, "SELECT * FROM unit");
                      echo "<option value=''> Semua </option>";
                      while ($a = mysqli_fetch_assoc($unit)) {
                        echo "<option value='".$a['namaunit']."'>".$a['namaunit']."</option>";
                      }
                  echo "</select>";
                ?>
            </div>
 
          <div style="clear:both;"></div>
              <button name="btncari" class="btn btn-info pull-right" type="submit">Cari Data</button> 
          <div style="clear:both;"></div>

          </form>

        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>NIP</th>
              <th>Nama Pegawai</th>
              <th>Tempat Lahir</th>
              <th>Tanggal Lahir</th>
              <th width="">Alamat</th>
              <th width="13%">Jenis Kelamin</th>
              <th width="5%">Golongan Darah</th>
              <th>Pendidikan</th>
              <th width="14%">Jabatan</th>
              <th>Unit</th>
              <th width="9%">Aksi</th>
             </tr>
          </thead>
          <tbody>
            <?php

            if(isset($_POST['btncari'])){
            $result = mysqli_query($conn, "SELECT * from pegawai 
              inner join pendidikan on pendidikan.id_pendidikan = pegawai.pendidikan 
              inner join jabatan on jabatan.id_jabatan = pegawai.jabatan
              inner join unit on unit.id_unit = pegawai.unit WHERE namaunit LIKE '%$_POST[unit]%' order by namapegawai asc");
             }else{
              $result = mysqli_query($conn, "SELECT * from pegawai
                inner join pendidikan on pendidikan.id_pendidikan = pegawai.pendidikan 
                inner join jabatan on jabatan.id_jabatan = pegawai.jabatan
                inner join unit on unit.id_unit = pegawai.unit order by namapegawai asc");
             }

            $jumlah = mysqli_num_rows($result);

              $i=1;

            if($jumlah > 0) {
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
				          <td id='np".$row['id_pegawai']."'>".$row['nip']."</td>
                  <td id='nm".$row['id_pegawai']."'>".$row['namapegawai']."</td>
                  <td id='tpl".$row['id_pegawai']."'>".$row['tempatlahir']."</td>
                  <td id='tgl".$row['id_pegawai']."'>".tanggal_format_indonesia($row['tanggallahir'])."</td>
                  <td id='al".$row['id_pegawai']."'>".$row['alamat']."</td>
                  <td id='jl".$row['id_pegawai']."'>".$row['jeniskelamin']."</td>
        				  <td id='gd".$row['id_pegawai']."'>".$row['goldarah']."</td>
        				  <td id='pd".$row['id_pegawai']."'>".$row['namapendidikan']."</td>
        				  <td id='jb".$row['id_pegawai']."'>".$row['namajabatan']."</td>
        				  <td id='un".$row['id_pegawai']."'>".$row['namaunit']."</td>
                  <td align='center'>
                   <span class='edit-p' data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_pegawai'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_pegawai'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>

<!-- session storage for selected dropdown -->
<script type="text/javascript">
  window.onload = function() {
    var selItem = sessionStorage.getItem("cariunit");  
    $('#cariunit').val(selItem);
    }
    $('#cariunit').change(function() { 
        var selVal = $(this).val();
        sessionStorage.setItem("cariunit", selVal);
    });
</script>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['nip'])){
    $id_pegawai = $_POST['id_pegawai'];
    $nip = $_POST['nip'];	  
    $namapegawai = $_POST['namapegawai'];
    $tempatlahir = $_POST['tempatlahir'];
    $tanggallahir = $_POST['tanggallahir'];
    $alamat = $_POST['alamat'];
	$jeniskelamin = $_POST['jeniskelamin'];
    $goldarah = $_POST['goldarah'];
	$pendidikan = $_POST['pendidikan'];
	$jabatan = $_POST['jabatan'];
    $unit = $_POST['unit'];

    if ($id_pegawai == "") {
      $query ="INSERT INTO pegawai (nip, namapegawai, tempatlahir, tanggallahir,  alamat, jeniskelamin , goldarah, pendidikan, jabatan, unit, aktif) VALUES ('$nip','$namapegawai','$tempatlahir','$tanggallahir','$alamat','$jeniskelamin','$goldarah','$pendidikan','$jabatan', '$unit', TRUE)";
      $info ="tambah";
    }
    elseif($id_pegawai != "") {
      $query = "update pegawai set nip='$nip',namapegawai='$namapegawai', tempatlahir='$tempatlahir', tanggallahir='$tanggallahir',alamat='$alamat' , jeniskelamin='$jeniskelamin' , goldarah='$goldarah',pendidikan='$pendidikan',jabatan='$jabatan' ,unit='$unit' where id_pegawai='$id_pegawai'";
      $info ="update";
    }
    
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=pegawai&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from pegawai where id_pegawai='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=pegawai&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $("#err-nip").hide();
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  
  function edit(id){
	np = $("#np"+id).text();
    nm = $("#nm"+id).text();
    tpl = $("#tpl"+id).text();
    tgl = $("#tgl"+id).text();
    al = $("#al"+id).text();
	jk = $("#jk"+id).text();
	gd = $("#gd"+id).text();
	pd = $("#pd"+id).text();
	jb = $("#jb"+id).text();
	un = $("#un"+id).text();  
	  
	$("#nip").val(np);
    $("#namapegawai").val(nm);
    $("#tempatlahir").val(tpl);
    $("#tanggallahir").val(tgl).change();
    $("#alamat").val(al);
	$("#jeniskelamin").val(jk).change();
    $("#goldarah").val(gd).change();
	$("#pendidikan").val(pd).change();
	$("#jabatan").val(jb).change();
	$("#unit").val(un).change();
	$("#id").val(id);
  }

  function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
   function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }

  function hapus(id){
   document.location.href = base_url+'index.php?page=pegawai&hapus='+id;
  }

$('.datepicker').datetimepicker({
  language:  'id',
  weekStart: 1,
  todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
});

$('#simpan').on('click', function(e) {
  
  var validator = $("#formiki").validate({
    rules: {
	  nip: {required: true, maxlength: 7},	
      namapegawai: {required: true},
      tempatlahir: {required: true},
      tanggallahir: {required: true},
      alamat: {required: true},
	  jeniskelamin: {required: true},
      goldarah: {required: true},
	  pendidikan: {required: true},
	  jabatan: {required: true},
	  unit: {required: true},
    },
    messages: {
	  nip: {required: "NIP tidak boleh kosong", maxlength: "NIP tidak boleh lebih dari 7 digit"},
      namapegawai: {required: "Nama tidak boleh kosong"},
      tempatlahir: {required: "Tempat lahir tidak boleh kosong"},
      tanggallahir: {required: "Tanggal lahir tidak boleh kosong"},
      alamat: {required: "alamat tidak boleh kosong"},
	  jeniskelamin: {required: "Jenis Kelamin tidak boleh kosong"},
      goldarah: {required: "Golongan Darah tidak boleh kosong"},
	  pendidikan: {required: "Pendidikan tidak boleh kosong"},
	  jabatan: {required: "Jabatan tidak boleh kosong"},
	  unit: {required: "Unit tidak boleh kosong"},
    }
  });

  if($("#formiki").valid()) {
    var self = $(this).parents("#formiki");
    $nip = self.find("#nip").val();
    //console.log ($nip);
    $aksiform = $("#aksi-h").val();
    if($aksiform === "add") {
    $.post(base_url + "aksi/cek_nip.php", {nip : $nip}, function(res){
      if(res !== "ada"){
        $("#err-nip").hide();
        //console.log(res);
        document.getElementById("formiki").submit();
      } else {
        //console.log(res);
        $("#err-nip").show();
        
      }
    }, "json");
    
    }else {
      document.getElementById("formiki").submit();
    }
    e.preventDefault();
  }
});
$("#tambah-p").on("click", function() {
  $("#aksi-h").val("add");
});
$("#tabel").on("click", ".edit-p", function() {
  $("#aksi-h").val("edit");
  $("#err-nip").hide();
});

</script>
