<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
  if(isset($_GET['info'])){
    switch ($_GET['info']) {
      case 'tambah':
        echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
        break;
      case 'update':
        echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
        break;
      case 'hapus':
        echo "<div class='alert alert-warning alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
        break;
      case 'gagal':
        echo "<div class='alert alert-warning alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data gagal disimpan</b></div>";
        break;
    }
  }
?>
<!-- tambah project -->
  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog" style="width: 70%">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form Project</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <form action="#" method="post" id="formiki" class="row">
            <div class=" col-md-offset-1 col-md-10 " style="">
                <div class="col-md-6">
                  <div class="form-group">
                    <label >Tipe Project</label>
                    <select name="tipe_proyek" class="form-control" id="tipe_proyek" name="tipe_proyek">
                        <option value="">--Silahkan Pilih tipe Proyek--</option>
                        <option value="P">Proyek Proffesional Service</option>
                        <option value="L">Proyek Lisensi</option>
                        <option value="S">Proyek Lisensi & Proffesional Service</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label >Kode Project</label>
                    <input type="text" class="form-control" placeholder="Kode Project" id="kode_proyek" name="kode_proyek" readonly="true"></input>
                  </div>

                  <div class="form-group">
                    <label >Nama Project</label>
                    <input type="text" class="form-control" placeholder="Nama Project" name="nama_proyek"></input>
                  </div>

                  <div class="form-group">
                    <label >Deskripsi</label>
                    <textarea name="deskripsi" placeholder="Deskripsi" class="form-control"></textarea>
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label >Nama Client</label>
                      <select name="id_client" class="form-control">
                        <option selected="" disabled="">Pilih Client</option>
                        <?php
                          $query=mysqli_query($conn, "select id_client, nama_client from client");
                          while ($data=mysqli_fetch_array($query)) {
                          echo "<option value=".$data['id_client']." >".$data['nama_client']."</option>";
                          }
                        ?>
                      </select>
                  </div>

                  <div class="form-group">
                    <label >Biaya</label>
                    <input type="text" class="form-control" placeholder="Rp." name="biaya"></input>
                  </div>

                  <div class="form-group">
                    <label >Tanggal Mulai</label>
                    <input type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd" placeholder="Tanggal Mulai" name="tanggal_mulai"></input>
                  </div>

                  <div class="form-group">
                    <label >Tanggal Selesai</label>
                    <input type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd" placeholder="Tanggal Selesai" name="tanggal_selesai"></input>
                  </div>
                </div>  
                

                <div style="" class=" text-center">
                  <button class="btn btn-primary" type="submit" name="kirim"> Simpan </button>
                  <button class="btn btn-default" type="reset"> Bersihkan </button>
                </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
<!-- selesai tambah project -->

<!-- tabel project -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Project</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' style="margin: 10px"><i class="fa fa-plus"></i> Tambah Project</button>

      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Proyek</th>
              <th>Nama Proyek</th>
              <th>Nama Client</th>
              <th>Biaya (Rp.)</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Selesai</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $query =" select p.*, c.id_client, c.nama_client
                        from proyek p
                        join client c
                        on p.id_client=c.id_client
                        order by p.created_at desc";
              $result = mysqli_query($conn, $query);
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>
                  <td align='center'>".$i."</td>
                  <td>".$row['kode_proyek']."</td>
                  <td>".$row['nama_proyek']."</td>
                  <td>".$row['nama_client']."</td>
                  <td>".format_rupiah($row['biaya'])."</td>
                  <td>".tanggal_format_indonesia($row['tanggal_mulai'])."</td>
                  <td>".tanggal_format_indonesia($row['tanggal_selesai'])."</td>
                  <td><label class='label label-".label_status($row['status'])."'>".$row['status']."</label></td>
                  <td align='center'>";
                    echo " <a class='btn btn-primary btn-sm' href='?page=detail.project&id=".$row['id']."'> <span class='glyphicon glyphicon-tasks' data-toggle='tooltip' data-original-title='Detail'></span> </a>&nbsp";

                    
                      echo "<a class='btn btn-success btn-sm' href='?page=progres.project&id=".$row['id']."'> <span class='glyphicon glyphicon-stats' data-toggle='tooltip' data-original-title='Progress'></span> </a>&nbsp";
                    
                   
                      echo "<span class='btn btn-danger btn-sm' onclick='hapus(".$row['id'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>";
                   

                   "</td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel project -->

<?php
  if(isset($_POST['kirim'])){

    $kode_proyek = $_POST['kode_proyek'];
    $nama_proyek = $_POST['nama_proyek'];
    $deskripsi = $_POST['deskripsi'];
    $biaya = $_POST['biaya'];
    $tanggal_mulai = $_POST['tanggal_mulai'];
    $tanggal_selesai = $_POST['tanggal_selesai'];
    $id_client = $_POST['id_client'];
	
	
   $query= "INSERT INTO proyek (kode_proyek, id_client, nama_proyek, deskripsi, biaya, tanggal_mulai, tanggal_selesai, status)
  		    VALUES ('$kode_proyek','$id_client', '$nama_proyek', '$deskripsi','$biaya', '$tanggal_mulai', '$tanggal_selesai', 'ON PROCESS' )";

	if(strtotime($tanggal_mulai)<strtotime($tanggal_selesai)){
    $result = mysqli_query($conn, $query);
	}
	else
	{
        echo "<script> document.location.href = base_url+'index.php?page=project&info=gagal'</script>";
	}
   //header('location:beranda.php?page=dataproyek.php');
        echo "<script> document.location.href = base_url+'index.php?page=project&info=tambah'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from proyek where id='$id'";
    $result = mysqli_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=listproject&req=detail&info=hapus'</script>";
  }
 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
$('#tipe_proyek').change(function() {
    var tipe = $(this).val();
    $.ajax({
        url: base_url + "lib.php",
        type: 'GET',
        data: {
            tipe: tipe,
            funct:"getNewCodeProject"
        },
        success: function(data)
        {
            $('#kode_proyek').val(data);
        }
    });
});

$(".alert" ).fadeOut(8000);
var validator = $("#formiki").validate({
  rules: {
    tipe_proyek: {required: true},
    nama_proyek: {required: true},
    id_client: {required: true},
    biaya: {required: true, number:true},
    tanggal_mulai: {deskripsi: true},
    tanggal_mulai: {required: true},
    tanggal_selesai: {required: true},
  },
  messages: {
    tipe_proyek: {required: "Tipe proyek harus dipilih"},
    nama_proyek: {required: "Nama proyek tidak boleh kosong"},
    id_client: {required: "Client tidak boleh kosong"},
    biaya: {required: "Biaya tidak boleh kosong", number:"Harus berupa angka"},
    tanggal_mulai: {required: "Tanggal mulai tidak boleh kosong"},
    deskripsi: {required: "Deskripsi tidak boleh kosong"},
    tanggal_selesai: {required: "Tanggal mulai tidak boleh kosong"},
  }
});

function hapus(id){
 document.location.href = base_url+'index.php?page=project&hapus='+id;
}

$('#tabel').dataTable();

$('.datepicker').datetimepicker({
  language:  'id',
  weekStart: 1,
  todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
});
</script>
