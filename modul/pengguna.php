<style>
  .input-group{
    padding:5px;
  }
  .error {
    color:red;
  }
</style>

<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
if(isset($_GET['info'])){
  switch ($_GET['info']) {
    case 'tambah':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil ditambahkan</b></div>";
      break;
    case 'update':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil diubah</b></div>";
      break;
    case 'hapus':
      echo "<div class='alert alert-success alert-dimissable'> <i class='fa fa-check'></i> <button class='close' type='button' data-dismiss='alert' aria-hidden='true'> x </button><b>Data berhasil dihapus</b></div>";
      break;
  }
}
?>
<!-- tambah user -->

  <div class="modal fade " role="dialog" id="myModal">
    <div class=" modal-dialog">
      <div class="box box-solid box-primary modal-content">

        <div class="box-header">
          <h3 class="box-title"><i class="ion-person-add"></i> &nbsp;Form Pengguna</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-dismiss="modal" style="font-size: 1.3em">&times;</button>
          </div>
        </div>

        <div class="box-body">
          <form action="" method="post" id="formiki" class="row">
            <div class="col-md-10 col-md-offset-1" style="">
              <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" placeholder="Nama" id="nama" name="nama" value=""/>
                <input type="text" id="id" name="id_user" value="" hidden></input>
              </div>
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" placeholder="Username" id="username" name="username"></input>
              </div>

              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" placeholder="Password" name="password"></input>
              </div>


              <div class="form-group">
                <label>Status</label>
                <select class="form-control" id="status" name="status">
                  <option value=""> - Pilih Status - </option>
                  <option value='KETUAKOPERASI'>KETUA KOPERASI</option>
                  <option value='KETUAUNIT'>KETUA UNIT</option>
                  <option value='KETENAGAKERJAAN'>KETENAGAKERJAAN</option>
               </select>
              </div>


               <div class="form-group">
               <label id="a" style="display: none">Unit</label>
                <?php
                  echo "<select class='form-control' id='pilunit' name='pilunit' style = 'display:none'>";
                    $unit = mysqli_query($conn, "select * from unit");
                  echo '<option value="" selected> - Pilih Jenis Unit - </option>';
                  while ($a = mysqli_fetch_assoc($unit)) {
                    echo "<option value='".$a['id_unit']."'>".$a['namaunit']."</option>";

                  }

                  echo "</select>";
                ?>

                <!-- form unit nya hilang kalo statusnya bukan KETUAUNIT -->
                <script type="text/javascript">
                  $('#status').change(function () {
                      if ($(this).find('option:selected').text() != 'KETUA UNIT') {
                          $("#pilunit").css("display", "none");
                          $("#a").css("display", "none");
                          $("#pilunit").val('');
                      } else {
                         $("#pilunit").css("display", "");
                         $("#a").css("display", "");
                      }
                  });
                </script>
              </div>
            </div>  
            <div style="" class=" text-center">
              <button class="btn btn-primary" type="submit" name="simpan"> Simpan </button>
              <button class="btn btn-default" type="reset"> Bersihkan </button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<!-- selesai tambah user -->

<!-- tabel user -->
<div class="row">
  <div class="col-lg-12">
    <div class="box " style="border-radius: 0;padding: 10px">

      <div class="box-header " style="border-radius: 0;border-bottom: 1px solid #eee;padding: 0;">
        <h3 class="box-title" ></i> Daftar Pengguna</h3>
      </div> <br>
      <button class="btn btn-primary" data-toggle='modal' data-target='#myModal' style="margin: 10px"><i class="fa fa-plus"></i> Tambah Pengguna</button>
      <div class="box-body table-responsive">
        <table id="tabel" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Nama Pengguna</th>
              <th>Username</th>
              <th>Status</th>
              <th width="9%">Aksi</th>
              
            </tr>
          </thead>
          <tbody>
            <?php
              $result = mysqli_query($conn, "select * from user");
              $i=1;
              while ($row = mysqli_fetch_assoc($result)) {
                 echo "<tr>
                  <td align='center'>".$i."</td>
                  <td id='nm".$row['id_user']."'>".$row['nama']."</td>
                  <td id='un".$row['id_user']."'>".$row['username']."</td>
                  <td id='st".$row['id_user']."'>".$row['status']."</td>
                  <td align='center'>
                    <span  data-toggle='modal' data-target='#myModal' onclick='edit(".$row['id_user'].")' ><a class='btn btn-primary btn-sm' style='' data-toggle='tooltip' data-original-title='Ubah Data'><i class='fa fa-edit'></i> </a></span> &nbsp;
                    <span class='btn btn-danger btn-sm' onclick='hapus(".$row['id_user'].")' data-toggle='tooltip' data-original-title='Hapus Data'><i class='fa fa-trash-o'></i> </span>
                  </td>
                </tr>";
                $i++;
              }
            ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- selesai tabel user -->

<?php
  if(isset($_POST['simpan'])){
    $id_user = $_POST['id_user'];
    $nama = $_POST['nama'];
    $username = $_POST['username'];
    $status = $_POST['status'];
    if($_POST['pilunit'] === ''){
    $idunit = $_POST['pilunit'] = 'NULL';
    }else{
      $idunit = $_POST['pilunit'];
    }
    $password = md5($_POST['password']);


    if ($id_user == "") {
      // $query ="INSERT INTO user (nama, username, password, status) VALUES ('$nama', '$username', '$password', '$status')";
      // $idunit = !empty($idunit) ? "'$idunit'" : "NULL";
      $query = "INSERT INTO user (nama,username, password, status)
        VALUES('$nama', '$username', '$password', '$status');
      INSERT INTO akses (id_user, id_unit) 
        VALUES(LAST_INSERT_ID(), $idunit);";
      $info ="tambah";
    }
    elseif($id_user != "" and $password =="") {
      $query = "update user set nama='$nama', username='$username', status='$status' where id_user='$id_user'";
      $info ="update";
    }
    elseif ($id_user != "" and $password != "") {
      $query = "UPDATE user set nama='$nama', username='$username', status='$status', password='$password' where id_user='$id_user'; UPDATE akses set id_unit = $idunit where id_user='$id_user'; ";
      $info ="update";
    }
    $result = mysqli_multi_query($conn, $query);
    echo "<script> document.location.href = base_url+'index.php?page=pengguna&info=".$info."'</script>";
  }

  if(isset($_GET['hapus'])){
    $id = $_GET['hapus'];
    $query = "delete from user where id_user='$id'";
    $result = mysqli_query($conn, $query);
    // print_r($result);die();
    echo "<script> document.location.href = base_url+'index.php?page=pengguna&info=hapus'</script>";
  }

 ?>

<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tabel').dataTable();
  $(".alert" ).fadeOut(8000);
  var validator = $("#formiki").validate({
    rules: {
      nama: {required: true},
      username: {required: true},
      password: {required: true, minlength:6},
      status: {required: true},
    },
    messages: {
      nama: {required: "Nama tidak boleh kosong"},
      username: {required: "Username tidak boleh kosong"},
      password: {required: "Password tidak boleh kosong", minlength:"Minimal 6 digit"},
      status: {required: "Status tidak boleh kosong"},
    }
  });
  function edit(id){
    nm = $("#nm"+id).text();
    un = $("#un"+id).text();
    st = $("#st"+id).text();

    $("#nama").val(nm);
    $("#username").val(un);
    $("#status").val(st).change();
    $("#id").val(id);
  }
function hapus(id) {
    swal({
    title: "Anda yakin?",
    text: "Anda yakin ingin menghapus user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    cancelButtonText: "Tidak",
    confirmButtonText: "Ya, saya yakin",
    closeOnConfirm: false
  },
  function(){
    swal("Berhasil dihapus!", "data yang anda pilih berhasil dihapus.", "success");
    hapus(id);
  });
  }
	
  function hapus(id){
   document.location.href = base_url+'index.php?page=pengguna&hapus='+id;
  }
</script>
