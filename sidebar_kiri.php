<aside class="left-side sidebar-offcanvas">

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel text-center" style="margin: 0px 25% -30px 10%">
            <div class="pull-left info ">
<p>
          <?php echo $_SESSION["nama"]; ?><p>

                  (<?php echo ucfirst(strtolower($_SESSION["status"]));?>) <p>

                  <?php

                  	if($_SESSION["status"] == "KETUAUNIT"){
                  			echo ucfirst(strtolower($_SESSION["namaunit"]));
                  		}

                  ?>


              </p>
          </div>
        </div>

<?php 
if($_SESSION["status"] == "KETUAKOPERASI"){
echo "<br>"; 
?>
        <!-- menu -->
        <ul class="sidebar-menu">
            <li class="">
              <a href="index.php" onclick="remove();">
                <i class="fa fa-dashboard"></i> <span>Halaman Utama</span>
              </a>
            </li>
            <li>
              <a href="?page=Mpegawai" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Pegawai</span>
              </a>
            </li>
            <li>
              <a href="?page=pegawai_tidak_aktif">
                <i class="fa fa-group"></i> <span>Pegawai Tidak Aktif</span>
              </a>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-briefcase"></i <span>Laporan</span>
                <span class="pull-right-container">
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="?page=laporan_penilaianpegawai_k" onclick="remove();"><i class="fa fa-circle-o"></i>Penilaian Pegawai</a></li>
                <li><a href="?page=laporan_mutasi" onclick="remove();"><i class="fa fa-circle-o"></i>Mutasi</a></li>
                <li><a href="?page=laporan_promosi" onclick="remove();"><i class="fa fa-circle-o"></i>Promosi</a></li>
                </ul>
              </ul>
            </li>
           
        </ul>
<?php } ?>
<?php
if($_SESSION["status"]=="KETUAUNIT"){
echo "<br>"; 
?>
        <ul class="sidebar-menu">
            <li class="">
              <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Halaman Utama</span>
              </a>
            </li>
            <li>
              <a href="?page=Mpegawaiketuaunit">
                <i class="fa fa-group"></i> <span>Manajemen Pegawai</span>
              </a>
            </li>
            <li>
              <a href="?page=pegawai_tidak_aktif">
                <i class="fa fa-group"></i> <span>Pegawai Tidak Aktif</span>
              </a>
            </li>
            <li>
              <a href="?page=penilaianpegawai">
                <i class="fa fa-briefcase"></i> <span>Penilaian Kinerja Pegawai</span>
              </a>
            </li>
            <li>
              <a href="?page=promosi">
                <i class="fa fa-briefcase"></i> <span>Promosi Pegawai</span>
              </a>
            </li>
            <li>
              <a href="?page=mutasi" onclick="remove();">
                <i class="fa fa-briefcase"></i> <span>Mutasi Pegawai</span>
              </a>
            </li>
        </ul>
<?php } ?>
<?php
if($_SESSION["status"] == "KETENAGAKERJAAN"){
echo "<br>" 
?>
        <ul class="sidebar-menu">
            <li class="">
              <a href="index.php" onclick="remove();">
                <i class="fa fa-dashboard"></i> <span>Halaman Utama</span>
              </a>
            </li>
            <li>
              <a href="?page=pengguna" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Pengguna</span>
              </a>
            </li>
            <li>
              <a href="?page=pegawai_tidak_aktif">
                <i class="fa fa-group"></i> <span>Pegawai Tidak Aktif</span>
              </a>
            </li>
			      <li>
              <a href="?page=pegawai" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Pegawai</span>
              </a>
            </li>
            <li>
              <a href="?page=pendidikan" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Pendidikan</span>
              </a>
            </li>
			<li>
              <a href="?page=unit" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Unit</span>
              </a>
            </li>
            <li>
              <a href="?page=jabatan" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Jabatan</span>
              </a>
            </li>
             <li>
              <a href="?page=kriteria" onclick="remove();">
                <i class="fa fa-group"></i> <span>Manajemen Kriteria</span>
              </a>
            </li>
            
               <li>
              <a href="?page=penilaiankriteria" onclick="remove();">
                <i class="fa fa-group"></i> <span>Data Penilaian Kriteria</span>
              </a>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-briefcase"></i <span>Laporan</span>
                <span class="pull-right-container">
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="?page=laporan_penilaianpegawai" onclick="remove();"><i class="fa fa-circle-o"></i>Penilaian Pegawai</a></li>
                <li><a href="?page=laporan_mutasi" onclick="remove();"><i class="fa fa-circle-o"></i>Mutasi</a></li>
                <li><a href="?page=laporan_promosi" onclick="remove();"><i class="fa fa-circle-o"></i>Promosi</a></li>
              </ul>
            </li>
        </ul>
<?php
}
?>
        <!-- selesai menu -->
    </section>

</aside>
<!-- selesai sidebar kiri menu -->

<script type="text/javascript">
function remove(){
  sessionStorage.removeItem("unit");
  sessionStorage.removeItem("cariunit");
  sessionStorage.removeItem("kopunit");
    sessionStorage.removeItem("projabatan");
  }

</script>
