<?php
  include_once("conf.php");

  function tanggal_format_indonesia($tgl, $waktu = FALSE, $bln_only = FALSE){
    $tanggal  =  substr($tgl,8,2);
    $bulan  =  getBulan(substr($tgl,5,2));
    $tahun  =  substr($tgl,0,4);
    $jam = substr($tgl, 11,2);
    $menit = substr($tgl, 14,2);
    $separator = empty($jam) ? '' : ':';
    $r_wkt = $waktu == FALSE ? '' : $jam.$separator.$menit;

    $tanggal_formatted = $tanggal.' '.$bulan.' '.$tahun.' '.$r_wkt;

    if($bln_only){
      $tanggal_formatted = $bulan.' '.$tahun;
    }

    return $tanggal_formatted;
  }

  function  getBulan($bln) {
      switch  ($bln){
          case  1:
              $bln = "Januari";
              break;
          case  2:
              $bln = "Februari";
              break;
          case  3:
              $bln = "Maret";
              break;
          case  4:
              $bln = "April";
              break;
          case  5:
              $bln = "Mei";
              break;
          case  6:
              $bln = "Juni";
              break;
          case  7:
              $bln = "Juli";
              break;
          case  8:
              $bln = "Agustus";
              break;
          case  9:
              $bln = "September";
              break;
          case  10:
              $bln = "Oktober";
              break;
          case  11:
              $bln = "November";
              break;
          case  12:
              $bln = "Desember";
              break;
      }

      return $bln;
  }

  function getHari($hari) {
      switch  ($hari){
          case  '7':
              $hari = "Minggu";
              break;
          case  '1':
              $hari = "Senin";
              break;
          case  '2':
              $hari = "Selasa";
              break;
          case  '3':
              $hari = "Rabu";
              break;
          case  '4':
              $hari = "Kami";
              break;
          case  '5':
              $hari = "Jumat";
              break;
          case  '6':
              $hari = "Sabtu";
              break;
      }

      return $hari;
  }

 
  

?>
